webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/Error/error500/error500.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\r\n  error500 works!\r\n</p>\r\n"

/***/ }),

/***/ "../../../../../src/app/Error/error500/error500.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/Error/error500/error500.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Error500Component; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Error500Component = (function () {
    function Error500Component() {
    }
    Error500Component.prototype.ngOnInit = function () {
    };
    Error500Component = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-error500',
            template: __webpack_require__("../../../../../src/app/Error/error500/error500.component.html"),
            styles: [__webpack_require__("../../../../../src/app/Error/error500/error500.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], Error500Component);
    return Error500Component;
}());



/***/ }),

/***/ "../../../../../src/app/app-material.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppMaterial; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AppMaterial = (function () {
    function AppMaterial() {
    }
    AppMaterial = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["c" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["b" /* MatAutocompleteModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["d" /* MatButtonToggleModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["f" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["g" /* MatChipsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["B" /* MatStepperModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["h" /* MatDatepickerModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["j" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["l" /* MatExpansionModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["m" /* MatGridListModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["n" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["o" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["p" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["q" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["r" /* MatPaginatorModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["s" /* MatProgressBarModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["t" /* MatProgressSpinnerModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["u" /* MatRadioModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["v" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["w" /* MatSidenavModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["y" /* MatSliderModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["x" /* MatSlideToggleModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["z" /* MatSnackBarModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["A" /* MatSortModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["D" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["C" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["F" /* MatTooltipModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["E" /* MatToolbarModule */],
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["c" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["b" /* MatAutocompleteModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["d" /* MatButtonToggleModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["e" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["f" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["g" /* MatChipsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["B" /* MatStepperModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["h" /* MatDatepickerModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["j" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["l" /* MatExpansionModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["m" /* MatGridListModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["n" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["o" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["p" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["q" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["r" /* MatPaginatorModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["s" /* MatProgressBarModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["t" /* MatProgressSpinnerModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["u" /* MatRadioModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["v" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["w" /* MatSidenavModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["y" /* MatSliderModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["x" /* MatSlideToggleModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["z" /* MatSnackBarModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["A" /* MatSortModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["D" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["C" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["F" /* MatTooltipModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["E" /* MatToolbarModule */],
            ]
        })
    ], AppMaterial);
    return AppMaterial;
}());



/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".width {width: 100%;}\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"background-color:#7B8D8E\">\r\n\r\n        <div class=\"navbar\"  *ngIf=\"nav.visible\">\r\n                <app-navbar></app-navbar>\r\n        </div> \r\n               <div class=\"container\" style=\"margin-top: 4%; background-color:#7B8D8E; margin-bottom: 0\"  >\r\n                 <router-outlet ></router-outlet>\r\n               </div>\r\n              \r\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_filter__ = __webpack_require__("../../../../rxjs-compat/_esm5/add/operator/filter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_auth_navbar_service__ = __webpack_require__("../../../../../src/app/services/auth/navbar.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import PerfectScrollbar from 'perfect-scrollbar';

var AppComponent = (function () {
    //@ViewChild(NavbarComponent) navbar: NavbarComponent;
    function AppComponent(location, router, nav) {
        this.location = location;
        this.router = router;
        this.nav = nav;
        this.yScrollStack = [];
    }
    AppComponent.prototype.ngOnInit = function () {
        $.material.init();
        var elemMainPanel = document.querySelector('.main-panel');
        var elemSidebar = document.querySelector('.sidebar .sidebar-wrapper');
        this.nav.show();
    };
    AppComponent.prototype.ngAfterViewInit = function () {
        this.runOnRouteChange();
    };
    AppComponent.prototype.runOnRouteChange = function () {
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac()) {
            var elemMainPanel = document.querySelector('.main-panel');
            // const ps = new PerfectScrollbar(elemMainPanel);
            // ps.update();
        }
    };
    AppComponent.prototype.isMac = function () {
        var bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common__["f" /* Location */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */], __WEBPACK_IMPORTED_MODULE_4_app_services_auth_navbar_service__["a" /* NavbarService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_routing__ = __webpack_require__("../../../../../src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_components_module__ = __webpack_require__("../../../../../src/app/components/components.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__node_modules_angularfire2__ = __webpack_require__("../../../../angularfire2/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__node_modules_angularfire2_database__ = __webpack_require__("../../../../angularfire2/database/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angularfire2_auth__ = __webpack_require__("../../../../angularfire2/auth/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angularfire2_firestore__ = __webpack_require__("../../../../angularfire2/firestore/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__error_error404_error404_component__ = __webpack_require__("../../../../../src/app/error/error404/error404.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__Error_error500_error500_component__ = __webpack_require__("../../../../../src/app/Error/error500/error500.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__main_connexion_connexion_component__ = __webpack_require__("../../../../../src/app/main/connexion/connexion.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__main_connexion_login_login_component__ = __webpack_require__("../../../../../src/app/main/connexion/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__main_connexion_signup_signup_component__ = __webpack_require__("../../../../../src/app/main/connexion/signup/signup.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_app_app_material__ = __webpack_require__("../../../../../src/app/app-material.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_angular_froala_wysiwyg__ = __webpack_require__("../../../../angular-froala-wysiwyg/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21_app_services_pipes_keys_pipe__ = __webpack_require__("../../../../../src/app/services/pipes/keys.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_app_services_auth_navbar_service__ = __webpack_require__("../../../../../src/app/services/auth/navbar.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__main_properties_properties_component__ = __webpack_require__("../../../../../src/app/main/properties/properties.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__main_properties_list_propertie_list_propertie_component__ = __webpack_require__("../../../../../src/app/main/properties/list-propertie/list-propertie.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__main_properties_popup_propertie_popup_propertie_component__ = __webpack_require__("../../../../../src/app/main/properties/popup-propertie/popup-propertie.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__main_mydata_mydata_component__ = __webpack_require__("../../../../../src/app/main/mydata/mydata.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__main_mydata_list_mydata_list_mydata_component__ = __webpack_require__("../../../../../src/app/main/mydata/list-mydata/list-mydata.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__main_mydata_popup_mydata_popup_mydata_component__ = __webpack_require__("../../../../../src/app/main/mydata/popup-mydata/popup-mydata.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__main_mydata_add_mydata_add_mydata_component__ = __webpack_require__("../../../../../src/app/main/mydata/add-mydata/add-mydata.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__main_myreports_myreports_component__ = __webpack_require__("../../../../../src/app/main/myreports/myreports.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__main_myreports_list_reports_list_reports_component__ = __webpack_require__("../../../../../src/app/main/myreports/list-reports/list-reports.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__main_new_analysis_new_analysis_component__ = __webpack_require__("../../../../../src/app/main/new-analysis/new-analysis.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__main_new_property_new_property_component__ = __webpack_require__("../../../../../src/app/main/new-property/new-property.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__main_new_data_entry_new_data_entry_component__ = __webpack_require__("../../../../../src/app/main/new-data-entry/new-data-entry.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__main_analysis_analysis_component__ = __webpack_require__("../../../../../src/app/main/analysis/analysis.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__main_list_user_list_user_component__ = __webpack_require__("../../../../../src/app/main/list-user/list-user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__main_langing_page_langing_page_component__ = __webpack_require__("../../../../../src/app/main/langing-page/langing-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__main_popup_data_year_data_year_component__ = __webpack_require__("../../../../../src/app/main/popup/data-year/data-year.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41_app_services_data_services_propertyCharacteristic_data_service__ = __webpack_require__("../../../../../src/app/services/data-services/propertyCharacteristic-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42_app_services_data_services_operatingExpense_data_service__ = __webpack_require__("../../../../../src/app/services/data-services/operatingExpense-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43_app_services_globals_service__ = __webpack_require__("../../../../../src/app/services/globals.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44_app_services_auth_af_service__ = __webpack_require__("../../../../../src/app/services/auth/af.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45_app_services_guards_auth_guard__ = __webpack_require__("../../../../../src/app/services/guards/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46_app_services_data_services_user_data_service__ = __webpack_require__("../../../../../src/app/services/data-services/user-data.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







// firebase npm install angularfire2 firebase --save





//component






//Material design

//editor de text npm install angular-froala-wysiwyg --save


//pipe

// npm install @agm/core --save

























var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["K" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_21_app_services_pipes_keys_pipe__["a" /* KeysPipe */],
                __WEBPACK_IMPORTED_MODULE_12__dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_13__error_error404_error404_component__["a" /* Error404Component */],
                __WEBPACK_IMPORTED_MODULE_14__Error_error500_error500_component__["a" /* Error500Component */],
                __WEBPACK_IMPORTED_MODULE_15__main_connexion_connexion_component__["a" /* ConnexionComponent */],
                __WEBPACK_IMPORTED_MODULE_16__main_connexion_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_17__main_connexion_signup_signup_component__["a" /* SignupComponent */],
                __WEBPACK_IMPORTED_MODULE_24__main_properties_properties_component__["a" /* PropertiesComponent */],
                __WEBPACK_IMPORTED_MODULE_25__main_properties_list_propertie_list_propertie_component__["a" /* ListPropertieComponent */],
                __WEBPACK_IMPORTED_MODULE_26__main_properties_popup_propertie_popup_propertie_component__["a" /* PopupPropertieComponent */],
                __WEBPACK_IMPORTED_MODULE_27__main_mydata_mydata_component__["a" /* MydataComponent */],
                __WEBPACK_IMPORTED_MODULE_28__main_mydata_list_mydata_list_mydata_component__["a" /* ListMydataComponent */],
                __WEBPACK_IMPORTED_MODULE_29__main_mydata_popup_mydata_popup_mydata_component__["a" /* PopupMydataComponent */],
                __WEBPACK_IMPORTED_MODULE_30__main_mydata_add_mydata_add_mydata_component__["a" /* AddMydataComponent */],
                __WEBPACK_IMPORTED_MODULE_31__main_myreports_myreports_component__["a" /* MyreportsComponent */],
                __WEBPACK_IMPORTED_MODULE_32__main_myreports_list_reports_list_reports_component__["a" /* ListReportsComponent */],
                __WEBPACK_IMPORTED_MODULE_33__main_new_analysis_new_analysis_component__["a" /* NewAnalysisComponent */],
                __WEBPACK_IMPORTED_MODULE_34__main_new_property_new_property_component__["a" /* NewPropertyComponent */],
                __WEBPACK_IMPORTED_MODULE_35__main_new_data_entry_new_data_entry_component__["a" /* NewDataEntryComponent */],
                __WEBPACK_IMPORTED_MODULE_36__main_analysis_analysis_component__["a" /* AnalysisComponent */],
                __WEBPACK_IMPORTED_MODULE_37__main_list_user_list_user_component__["a" /* ListUserComponent */],
                __WEBPACK_IMPORTED_MODULE_38__main_langing_page_langing_page_component__["a" /* LangingPageComponent */],
                __WEBPACK_IMPORTED_MODULE_40__main_popup_data_year_data_year_component__["a" /* DataYearComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_18_app_app_material__["a" /* AppMaterial */],
                __WEBPACK_IMPORTED_MODULE_20__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_5__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* RouterModule */],
                __WEBPACK_IMPORTED_MODULE_4__app_routing__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_8__node_modules_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_7__environments_environment__["a" /* environment */].firebase),
                __WEBPACK_IMPORTED_MODULE_9__node_modules_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_10_angularfire2_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_11_angularfire2_firestore__["b" /* AngularFirestoreModule */],
                __WEBPACK_IMPORTED_MODULE_22__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyAOAB3zEWzZok5s45SvIxBf9u1SIY1rf8A'
                }),
                __WEBPACK_IMPORTED_MODULE_19_angular_froala_wysiwyg__["a" /* FroalaEditorModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_19_angular_froala_wysiwyg__["b" /* FroalaViewModule */].forRoot()
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_45_app_services_guards_auth_guard__["a" /* AuthGuard */], __WEBPACK_IMPORTED_MODULE_44_app_services_auth_af_service__["a" /* AfService */], __WEBPACK_IMPORTED_MODULE_41_app_services_data_services_propertyCharacteristic_data_service__["a" /* PropertyCharacteristicDataService */], __WEBPACK_IMPORTED_MODULE_42_app_services_data_services_operatingExpense_data_service__["a" /* OperatingExpenseDataService */], __WEBPACK_IMPORTED_MODULE_46_app_services_data_services_user_data_service__["a" /* UserDataService */], __WEBPACK_IMPORTED_MODULE_43_app_services_globals_service__["a" /* GlobalsService */], __WEBPACK_IMPORTED_MODULE_23_app_services_auth_navbar_service__["a" /* NavbarService */], { provide: __WEBPACK_IMPORTED_MODULE_39__angular_common__["g" /* LocationStrategy */], useClass: __WEBPACK_IMPORTED_MODULE_39__angular_common__["d" /* HashLocationStrategy */] }
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_40__main_popup_data_year_data_year_component__["a" /* DataYearComponent */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__error_error404_error404_component__ = __webpack_require__("../../../../../src/app/error/error404/error404.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Error_error500_error500_component__ = __webpack_require__("../../../../../src/app/Error/error500/error500.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_connexion_connexion_component__ = __webpack_require__("../../../../../src/app/main/connexion/connexion.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__main_properties_list_propertie_list_propertie_component__ = __webpack_require__("../../../../../src/app/main/properties/list-propertie/list-propertie.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__main_new_analysis_new_analysis_component__ = __webpack_require__("../../../../../src/app/main/new-analysis/new-analysis.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__main_new_property_new_property_component__ = __webpack_require__("../../../../../src/app/main/new-property/new-property.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__main_new_data_entry_new_data_entry_component__ = __webpack_require__("../../../../../src/app/main/new-data-entry/new-data-entry.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__main_analysis_analysis_component__ = __webpack_require__("../../../../../src/app/main/analysis/analysis.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__main_list_user_list_user_component__ = __webpack_require__("../../../../../src/app/main/list-user/list-user.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__main_myreports_myreports_component__ = __webpack_require__("../../../../../src/app/main/myreports/myreports.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__main_langing_page_langing_page_component__ = __webpack_require__("../../../../../src/app/main/langing-page/langing-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__main_mydata_mydata_component__ = __webpack_require__("../../../../../src/app/main/mydata/mydata.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_app_services_guards_auth_guard__ = __webpack_require__("../../../../../src/app/services/guards/auth.guard.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




//component 













var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_6__main_connexion_connexion_component__["a" /* ConnexionComponent */] },
    { path: 'home', component: __WEBPACK_IMPORTED_MODULE_14__main_langing_page_langing_page_component__["a" /* LangingPageComponent */], children: [
            { path: '', redirectTo: 'new-analysis', pathMatch: 'full' },
            { path: 'new-analysis', component: __WEBPACK_IMPORTED_MODULE_8__main_new_analysis_new_analysis_component__["a" /* NewAnalysisComponent */] },
            { path: 'new-property', component: __WEBPACK_IMPORTED_MODULE_9__main_new_property_new_property_component__["a" /* NewPropertyComponent */] },
            { path: 'new-data-entry', component: __WEBPACK_IMPORTED_MODULE_10__main_new_data_entry_new_data_entry_component__["a" /* NewDataEntryComponent */] },
            { path: 'analysis', component: __WEBPACK_IMPORTED_MODULE_11__main_analysis_analysis_component__["a" /* AnalysisComponent */] }
        ], canActivate: [__WEBPACK_IMPORTED_MODULE_16_app_services_guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'mydata', component: __WEBPACK_IMPORTED_MODULE_15__main_mydata_mydata_component__["a" /* MydataComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16_app_services_guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'new-analysis', component: __WEBPACK_IMPORTED_MODULE_8__main_new_analysis_new_analysis_component__["a" /* NewAnalysisComponent */] },
    { path: 'new-property', component: __WEBPACK_IMPORTED_MODULE_9__main_new_property_new_property_component__["a" /* NewPropertyComponent */] },
    { path: 'myreports', component: __WEBPACK_IMPORTED_MODULE_13__main_myreports_myreports_component__["a" /* MyreportsComponent */] },
    { path: 'new-data-entry', component: __WEBPACK_IMPORTED_MODULE_10__main_new_data_entry_new_data_entry_component__["a" /* NewDataEntryComponent */] },
    { path: 'analysis', component: __WEBPACK_IMPORTED_MODULE_11__main_analysis_analysis_component__["a" /* AnalysisComponent */] },
    { path: 'list-user', component: __WEBPACK_IMPORTED_MODULE_12__main_list_user_list_user_component__["a" /* ListUserComponent */] },
    { path: 'listPropertis', component: __WEBPACK_IMPORTED_MODULE_7__main_properties_list_propertie_list_propertie_component__["a" /* ListPropertieComponent */] },
    { path: 'errors/error-404', component: __WEBPACK_IMPORTED_MODULE_4__error_error404_error404_component__["a" /* Error404Component */], canActivate: [__WEBPACK_IMPORTED_MODULE_16_app_services_guards_auth_guard__["a" /* AuthGuard */]] },
    { path: 'errors/error-500', component: __WEBPACK_IMPORTED_MODULE_5__Error_error500_error500_component__["a" /* Error500Component */], canActivate: [__WEBPACK_IMPORTED_MODULE_16_app_services_guards_auth_guard__["a" /* AuthGuard */]] },
    { path: '**', redirectTo: 'home' }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* RouterModule */].forRoot(routes, { useHash: true })
            ],
            exports: [],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/components/components.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__footer_footer_component__ = __webpack_require__("../../../../../src/app/components/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__navbar_navbar_component__ = __webpack_require__("../../../../../src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__sidebar_sidebar_component__ = __webpack_require__("../../../../../src/app/components/sidebar/sidebar.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsModule = (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */],
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_4__navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_5__sidebar_sidebar_component__["b" /* SidebarComponent */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_4__navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_5__sidebar_sidebar_component__["b" /* SidebarComponent */]
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());



/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer>\r\n    <div class=\"container-fluid\">\r\n\r\n    </div>\r\n</footer>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = (function () {
    function FooterComponent() {
        this.test = new Date();
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/components/footer/footer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".form-control::-webkit-input-placeholder { color: white !important; }  /* WebKit, Blink, Edge */  .form-control:-moz-placeholder { color: white !important; }  /* Mozilla Firefox 4 to 18 */  .form-control::-moz-placeholder { color: white !important; }  /* Mozilla Firefox 19+ */  .form-control:-ms-input-placeholder { color: white !important; }  /* Internet Explorer 10-11 */  .form-control::-ms-input-placeholder { color: white !important; }  /* Microsoft Edge */  .color-menu:hover { \r\n    background-color: #E45641 !important;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-absolute\">\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"collapse navbar-collapse\">\r\n            <ul class=\"nav navbar-nav navbar-left\">\r\n                    <li>\r\n                         <a [routerLink]=\"'/Home'\">   \r\n                             <span style=\"          font-family: -webkit-pictograph;\r\n                            font-size: 200%;\r\n                            Font-Weight: Bold;\">PRO-COSTS</span>\r\n                        </a>\r\n                        </li>\r\n                        <li>\r\n                            <a [routerLink]=\"'/myreports'\" >\r\n                                <span style=\"color: #E45641\">My Reports</span>\r\n                            </a>\r\n                    \r\n                        </li>\r\n                        <li>\r\n                                <a [routerLink]=\"'/list-user'\" >\r\n                                    <span style=\"color: #E45641\">user management</span>\r\n                                </a>\r\n                        \r\n                        </li>\r\n            </ul>\r\n            <ul class=\"nav navbar-nav navbar-right\">\r\n                \r\n                        <li class=\"dropdown\">\r\n                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\r\n                        <i class=\"material-icons\" style=\"font-size: xx-large;\">person</i>\r\n                        <p class=\"hidden-lg hidden-md\">Profile</p>\r\n                    </a>\r\n                    <ul class=\"dropdown-menu\">\r\n                        <li><a class=\"color-menu\" href=\"#\">User: {{loggedInUser}}</a></li>\r\n                        <li><a class=\"color-menu\"  href=\"#\">Role: Property Owner</a></li>\r\n                        <li><a class=\"color-menu\" href=\"#\">Account: Test</a></li>\r\n                        <li><a class=\"color-menu\" (click)=\"onLogoutClick()\">logout</a></li>\r\n                    </ul>\r\n                </li>\r\n            </ul>\r\n\r\n            <form class=\"navbar-form navbar-right\" role=\"search\">\r\n                <div class=\"form-group form-black  is-empty\">\r\n                    <input type=\"text\" class=\"form-control bg-warning\" placeholder=\"Search\">\r\n                    <span class=\"material-input\"></span>\r\n                </div>\r\n                <button type=\"submit\" class=\"btn btn-white btn-round btn-just-icon\">\r\n                    <i class=\"material-icons\">search</i><div class=\"ripple-container\"></div>\r\n                </button>\r\n            </form>\r\n        </div>\r\n    </div>\r\n</nav>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sidebar_sidebar_component__ = __webpack_require__("../../../../../src/app/components/sidebar/sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_auth_navbar_service__ = __webpack_require__("../../../../../src/app/services/auth/navbar.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_auth_af_service__ = __webpack_require__("../../../../../src/app/services/auth/af.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NavbarComponent = (function () {
    function NavbarComponent(authService, router, location, element, nav) {
        this.authService = authService;
        this.router = router;
        this.element = element;
        this.nav = nav;
        this.location = location;
        this.sidebarVisible = false;
    }
    NavbarComponent.prototype.onLogoutClick = function () {
        this.authService.logout();
        this.router.navigate(['/']);
    };
    NavbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.listTitles = __WEBPACK_IMPORTED_MODULE_1__sidebar_sidebar_component__["a" /* ROUTES */].filter(function (listTitle) { return listTitle; });
        var navbar = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        this.authService.getAuth().subscribe(function (auth) { return _this.loggedInUser = auth.email; });
    };
    NavbarComponent.prototype.sidebarOpen = function () {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');
        this.sidebarVisible = true;
    };
    ;
    NavbarComponent.prototype.sidebarClose = function () {
        var body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    ;
    NavbarComponent.prototype.sidebarToggle = function () {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        }
        else {
            this.sidebarClose();
        }
    };
    ;
    NavbarComponent.prototype.getTitle = function () {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(2);
        }
        titlee = titlee.split('/').pop();
        for (var item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === titlee) {
                return this.listTitles[item].title;
            }
        }
        return '';
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-navbar',
            template: __webpack_require__("../../../../../src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_app_services_auth_af_service__["a" /* AfService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* Router */], __WEBPACK_IMPORTED_MODULE_2__angular_common__["f" /* Location */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_3_app_services_auth_navbar_service__["a" /* NavbarService */]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/sidebar/sidebar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".sidebar .nav .caret {\r\n    margin-top: 13px;\r\n    position: absolute;\r\n    right: 18px;\r\n}\r\n\r\n.caret {\r\n    display: inline-block;\r\n    width: 0;\r\n    height: 0;\r\n    margin-left: 2px;\r\n    vertical-align: middle;\r\n    border-top: 4px dashed;\r\n    border-top: 4px solid\\9;\r\n    border-right: 4px solid transparent;\r\n    border-left: 4px solid transparent;\r\n    margin-top: 13px;\r\n    position: absolute;\r\n    right: 18px;\r\n}\r\n\r\n.sidebar li.active>a {\r\n    background-color: #fff;\r\n    -webkit-box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px hsla(0,0%,100%,.4);\r\n    box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px hsla(0,0%,100%,.4);\r\n}\r\n\r\n.sidebar-mini {\r\n    text-transform: uppercase;\r\n    width: 30px;\r\n    margin-right: 15px;\r\n    float: left;\r\n    text-align: center;\r\n    letter-spacing: 1px;\r\n    position: relative;\r\n    display: inherit;\r\n}\r\n\r\n.customNav {\r\n    margin-top: -10px;\r\n    margin-left: 10px;\r\n    margin-right: 10px;\r\n}\r\n\r\n.sidebar .nav li.notactive a{\r\n    background-color: #fefefd00;\r\n}\r\n\r\nli .notactive > a {\r\n    background-color: #fefefd00;\r\n    -webkit-box-shadow: 0 12px 20px -10px rgba(132, 123, 123, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(151, 135, 134, 0);\r\n    box-shadow: 0 0px 0px -4px rgba(132, 123, 123, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(151, 135, 134, 0);\r\n}\r\n\r\nli .active > a {\r\n    background-color: #6d8794;\r\n    -webkit-box-shadow: 0 12px 20px -10px rgb(67,84,92), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(244, 67, 54, 0.2);\r\n    box-shadow: 0 12px 20px -10px rgb(67,84,92), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(244, 67, 54, 0.2);\r\n}\r\n\r\n.btn-group.bootstrap-select.open .caret, .dropdown.open .caret, .dropup.open .caret, a[data-toggle=collapse][aria-expanded=true] .caret {\r\n    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=2);\r\n    -webkit-transform: rotate(180deg);\r\n    transform: rotate(180deg);\r\n}\r\n\r\n.btn-group.bootstrap-select.open .caret, .dropdown.open .caret, .dropup.open .caret, a[data-toggle=collapse1][aria-expanded=true] .caret {\r\n    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=2);\r\n    -webkit-transform: rotate(180deg);\r\n    transform: rotate(180deg);\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/sidebar/sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"logo\"  >\r\n    <a href=\"\" class=\"simple-text\">\r\n   \r\n        <span style=\"          font-family: -webkit-pictograph;\r\n        font-size: 188%;\r\n        Font-Weight: Bold;\">PRO-COSTS</span> \r\n     <!-- <div> <img src=\"../assets/logo.gif\"></div>  -->\r\n    </a>\r\n</div>\r\n<div class=\"sidebar-wrapper\"   >\r\n \r\n\r\n    <ul class=\"nav nav-mobile-menu\" *ngIf=\"isMobileMenu()\">\r\n     \r\n       \r\n        <li>\r\n            <a href=\"#pablo\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\r\n               <i class=\"material-icons\">person</i>\r\n               <p class=\"hidden-lg hidden-md\">Profile</p>\r\n            </a>\r\n        </li>\r\n    </ul>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/sidebar/sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_services_auth_navbar_service__ = __webpack_require__("../../../../../src/app/services/auth/navbar.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ROUTES = [
    { path: 'PropertyManager2', title: 'My Properties list', icon: 'history', class: '' },
    { path: 'PropertyManager3', title: 'My Historical Data', icon: 'watch later', class: '' },
    { path: 'appraisal1', title: 'My Reports / Analysis', icon: 'dashboard', class: '' },
];
var SidebarComponent = (function () {
    function SidebarComponent(nav) {
        this.nav = nav;
        this.showNav = true;
    }
    SidebarComponent.prototype.ngOnInit = function () {
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.nav.hide();
    };
    SidebarComponent.prototype.isMobileMenu = function () {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
    ;
    SidebarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-sidebar',
            template: __webpack_require__("../../../../../src/app/components/sidebar/sidebar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/sidebar/sidebar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_app_services_auth_navbar_service__["a" /* NavbarService */]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n\r\n        <div class=\"container-fluid\">\r\n            <div class=\"row\">\r\n                <div class=\"col-lg-3 col-md-6 col-sm-6\">\r\n                    <div class=\"card card-stats\" >\r\n                        <div class=\"card-header\" data-background-color=\"orange\">\r\n                            <i class=\"material-icons\">content_copy</i>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <p class=\"category\">Used Space</p>\r\n                            <h3 class=\"title\">49/50<small>GB</small></h3>\r\n                        </div>\r\n                        <div class=\"card-footer\">\r\n                            <div class=\"stats\">\r\n                                <i class=\"material-icons text-danger\">warning</i> <a href=\"#pablo\">Get More Space...</a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-3 col-md-6 col-sm-6\">\r\n                    <div class=\"card card-stats\" >\r\n                        <div class=\"card-header\" data-background-color=\"green\">\r\n                            <i class=\"material-icons\">store</i>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <p class=\"category\">Revenue</p>\r\n                            <h3 class=\"title\">$34,245</h3>\r\n                        </div>\r\n                        <div class=\"card-footer\">\r\n                            <div class=\"stats\">\r\n                                <i class=\"material-icons\">date_range</i> Last 24 Hours\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-3 col-md-6 col-sm-6\">\r\n                    <div class=\"card card-stats\" >\r\n                        <div class=\"card-header\" data-background-color=\"appraisal\">\r\n                            <i class=\"material-icons\">info_outline</i>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <p class=\"category\">Fixed Issues</p>\r\n                            <h3 class=\"title\">75</h3>\r\n                        </div>\r\n                        <div class=\"card-footer\">\r\n                            <div class=\"stats\">\r\n                                <i class=\"material-icons\">local_offer</i> Tracked from Github\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-lg-3 col-md-6 col-sm-6\">\r\n                    <div class=\"card card-stats\" >\r\n                        <div class=\"card-header\" data-background-color=\"blue\">\r\n                            <i class=\"fa fa-twitter\"></i>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <p class=\"category\">Followers</p>\r\n                            <h3 class=\"title\">+245</h3>\r\n                        </div>\r\n                        <div class=\"card-footer\">\r\n                            <div class=\"stats\">\r\n                                <i class=\"material-icons\">update</i> Just Updated\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card\" >\r\n                        <div class=\"card-header card-chart\" data-background-color=\"green\">\r\n                            <div class=\"ct-chart\" id=\"dailySalesChart\"></div>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <h4 class=\"title\">Daily Sales</h4>\r\n                            <p class=\"category\"><span class=\"text-success\"><i class=\"fa fa-long-arrow-up\"></i> 55%  </span> increase in today sales.</p>\r\n                        </div>\r\n                        <div class=\"card-footer\">\r\n                            <div class=\"stats\">\r\n                                <i class=\"material-icons\">access_time</i> updated 4 minutes ago\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card\" >\r\n                        <div class=\"card-header card-chart\" data-background-color=\"orange\">\r\n                            <div class=\"ct-chart\" id=\"emailsSubscriptionChart\"></div>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <h4 class=\"title\">Email Subscriptions</h4>\r\n                            <p class=\"category\">Last Campaign Performance</p>\r\n                        </div>\r\n                        <div class=\"card-footer\">\r\n                            <div class=\"stats\">\r\n                                <i class=\"material-icons\">access_time</i> campaign sent 2 days ago\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"card\" >\r\n                        <div class=\"card-header card-chart\" data-background-color=\"appraisal\">\r\n                            <div class=\"ct-chart\" id=\"completedTasksChart\"></div>\r\n                        </div>\r\n                        <div class=\"card-content\">\r\n                            <h4 class=\"title\">Completed Tasks</h4>\r\n                            <p class=\"category\">Last Campaign Performance</p>\r\n                        </div>\r\n                        <div class=\"card-footer\">\r\n                            <div class=\"stats\">\r\n                                <i class=\"material-icons\">access_time</i> campaign sent 2 days ago\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n                <div class=\"col-lg-6 col-md-12\">\r\n                    <div class=\"card card-nav-tabs\" >\r\n                        <div class=\"card-header\" data-background-color=\"purple\">\r\n                            <div class=\"nav-tabs-navigation\">\r\n                                <div class=\"nav-tabs-wrapper\">\r\n                                    <span class=\"nav-tabs-title\">Tasks:</span>\r\n                                    <ul class=\"nav nav-tabs\" data-tabs=\"tabs\">\r\n                                        <li class=\"active\">\r\n                                            <a href=\"#profile\" data-toggle=\"tab\">\r\n                                                <i class=\"material-icons\">bug_report</i>\r\n                                                Bugs\r\n                                            <div class=\"ripple-container\"></div></a>\r\n                                        </li>\r\n                                        <li class=\"\">\r\n                                            <a href=\"#messages\" data-toggle=\"tab\">\r\n                                                <i class=\"material-icons\">code</i>\r\n                                                Website\r\n                                            <div class=\"ripple-container\"></div></a>\r\n                                        </li>\r\n                                        <li class=\"\">\r\n                                            <a href=\"#settings\" data-toggle=\"tab\">\r\n                                                <i class=\"material-icons\">cloud</i>\r\n                                                Server\r\n                                            <div class=\"ripple-container\"></div></a>\r\n                                        </li>\r\n                                    </ul>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"card-content\">\r\n                            <div class=\"tab-content\">\r\n                                <div class=\"tab-pane active\" id=\"profile\">\r\n                                    <table class=\"table\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td>\r\n                                                    <div class=\"checkbox\">\r\n                                                        <label>\r\n                                                            <input type=\"checkbox\" name=\"optionsCheckboxes\" checked>\r\n                                                        </label>\r\n                                                    </div>\r\n                                                </td>\r\n                                                <td>Sign contract for \"What are conference organizers afraid of?\"</td>\r\n                                                <td class=\"td-actions text-right\">\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Edit Task\" class=\"btn btn-primary btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">edit</i>\r\n                                                    </button>\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Remove\" class=\"btn btn-danger btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">close</i>\r\n                                                    </button>\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>\r\n                                                    <div class=\"checkbox\">\r\n                                                        <label>\r\n                                                            <input type=\"checkbox\" name=\"optionsCheckboxes\">\r\n                                                        </label>\r\n                                                    </div>\r\n                                                </td>\r\n                                                <td>Lines From Great Russian Literature? Or E-mails From My Boss?</td>\r\n                                                <td class=\"td-actions text-right\">\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Edit Task\" class=\"btn btn-primary btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">edit</i>\r\n                                                    </button>\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Remove\" class=\"btn btn-danger btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">close</i>\r\n                                                    </button>\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>\r\n                                                    <div class=\"checkbox\">\r\n                                                        <label>\r\n                                                            <input type=\"checkbox\" name=\"optionsCheckboxes\">\r\n                                                        </label>\r\n                                                    </div>\r\n                                                </td>\r\n                                                <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit\r\n                                                </td>\r\n                                                <td class=\"td-actions text-right\">\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Edit Task\" class=\"btn btn-primary btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">edit</i>\r\n                                                    </button>\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Remove\" class=\"btn btn-danger btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">close</i>\r\n                                                    </button>\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>\r\n                                                    <div class=\"checkbox\">\r\n                                                        <label>\r\n                                                            <input type=\"checkbox\" name=\"optionsCheckboxes\" checked>\r\n                                                        </label>\r\n                                                    </div>\r\n                                                </td>\r\n                                                <td>Create 4 Invisible User Experiences you Never Knew About</td>\r\n                                                <td class=\"td-actions text-right\">\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Edit Task\" class=\"btn btn-primary btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">edit</i>\r\n                                                    </button>\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Remove\" class=\"btn btn-danger btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">close</i>\r\n                                                    </button>\r\n                                                </td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                                <div class=\"tab-pane\" id=\"messages\">\r\n                                    <table class=\"table\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td>\r\n                                                    <div class=\"checkbox\">\r\n                                                        <label>\r\n                                                            <input type=\"checkbox\" name=\"optionsCheckboxes\" checked>\r\n                                                        </label>\r\n                                                    </div>\r\n                                                </td>\r\n                                                <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit\r\n                                                </td>\r\n                                                <td class=\"td-actions text-right\">\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Edit Task\" class=\"btn btn-primary btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">edit</i>\r\n                                                    </button>\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Remove\" class=\"btn btn-danger btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">close</i>\r\n                                                    </button>\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>\r\n                                                    <div class=\"checkbox\">\r\n                                                        <label>\r\n                                                            <input type=\"checkbox\" name=\"optionsCheckboxes\">\r\n                                                        </label>\r\n                                                    </div>\r\n                                                </td>\r\n                                                <td>Sign contract for \"What are conference organizers afraid of?\"</td>\r\n                                                <td class=\"td-actions text-right\">\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Edit Task\" class=\"btn btn-primary btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">edit</i>\r\n                                                    </button>\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Remove\" class=\"btn btn-danger btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">close</i>\r\n                                                    </button>\r\n                                                </td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                                <div class=\"tab-pane\" id=\"settings\">\r\n                                    <table class=\"table\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td>\r\n                                                    <div class=\"checkbox\">\r\n                                                        <label>\r\n                                                            <input type=\"checkbox\" name=\"optionsCheckboxes\">\r\n                                                        </label>\r\n                                                    </div>\r\n                                                </td>\r\n                                                <td>Lines From Great Russian Literature? Or E-mails From My Boss?</td>\r\n                                                <td class=\"td-actions text-right\">\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Edit Task\" class=\"btn btn-primary btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">edit</i>\r\n                                                    </button>\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Remove\" class=\"btn btn-danger btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">close</i>\r\n                                                    </button>\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>\r\n                                                    <div class=\"checkbox\">\r\n                                                        <label>\r\n                                                            <input type=\"checkbox\" name=\"optionsCheckboxes\" checked>\r\n                                                        </label>\r\n                                                    </div>\r\n                                                </td>\r\n                                                <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit\r\n                                                </td>\r\n                                                <td class=\"td-actions text-right\">\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Edit Task\" class=\"btn btn-primary btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">edit</i>\r\n                                                    </button>\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Remove\" class=\"btn btn-danger btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">close</i>\r\n                                                    </button>\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>\r\n                                                    <div class=\"checkbox\">\r\n                                                        <label>\r\n                                                            <input type=\"checkbox\" name=\"optionsCheckboxes\">\r\n                                                        </label>\r\n                                                    </div>\r\n                                                </td>\r\n                                                <td>Sign contract for \"What are conference organizers afraid of?\"</td>\r\n                                                <td class=\"td-actions text-right\">\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Edit Task\" class=\"btn btn-primary btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">edit</i>\r\n                                                    </button>\r\n                                                    <button type=\"button\" rel=\"tooltip\" title=\"Remove\" class=\"btn btn-danger btn-simple btn-xs\">\r\n                                                        <i class=\"material-icons\">close</i>\r\n                                                    </button>\r\n                                                </td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"col-lg-6 col-md-12\">\r\n                    <div class=\"card\" >\r\n                        <div class=\"card-header\" data-background-color=\"orange\">\r\n                            <h4 class=\"title\">Employees Stats</h4>\r\n                            <p class=\"category\">New employees on 15th September, 2016</p>\r\n                        </div>\r\n                        <div class=\"card-content table-responsive\">\r\n                            <table class=\"table table-hover\">\r\n                                <thead class=\"text-warning\">\r\n                                    <tr>\r\n                                        <th>ID</th>\r\n                                        <th>Name</th>\r\n                                        <th>Salary</th>\r\n                                        <th>Country</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <td>1</td>\r\n                                        <td>Dakota Rice</td>\r\n                                        <td>$36,738</td>\r\n                                        <td>Niger</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>2</td>\r\n                                        <td>Minerva Hooper</td>\r\n                                        <td>$23,789</td>\r\n                                        <td>Curaçao</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>3</td>\r\n                                        <td>Sage Rodriguez</td>\r\n                                        <td>$56,142</td>\r\n                                        <td>Netherlands</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>4</td>\r\n                                        <td>Philip Chaney</td>\r\n                                        <td>$38,735</td>\r\n                                        <td>Korea, South</td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_chartist__ = __webpack_require__("../../../../chartist/dist/chartist.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_chartist___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_chartist__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DashboardComponent = (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.startAnimationForLineChart = function (chart) {
        var seq, delays, durations;
        seq = 0;
        delays = 80;
        durations = 500;
        chart.on('draw', function (data) {
            if (data.type === 'line' || data.type === 'area') {
                data.element.animate({
                    d: {
                        begin: 600,
                        dur: 700,
                        from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                        to: data.path.clone().stringify(),
                        easing: __WEBPACK_IMPORTED_MODULE_1_chartist__["Svg"].Easing.easeOutQuint
                    }
                });
            }
            else if (data.type === 'point') {
                seq++;
                data.element.animate({
                    opacity: {
                        begin: seq * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }
        });
        seq = 0;
    };
    ;
    DashboardComponent.prototype.startAnimationForBarChart = function (chart) {
        var seq2, delays2, durations2;
        seq2 = 0;
        delays2 = 80;
        durations2 = 500;
        chart.on('draw', function (data) {
            if (data.type === 'bar') {
                seq2++;
                data.element.animate({
                    opacity: {
                        begin: seq2 * delays2,
                        dur: durations2,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }
        });
        seq2 = 0;
    };
    ;
    DashboardComponent.prototype.ngOnInit = function () {
        /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */
        var dataDailySalesChart = {
            labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
            series: [
                [12, 17, 7, 17, 23, 18, 38]
            ]
        };
        var optionsDailySalesChart = {
            lineSmooth: __WEBPACK_IMPORTED_MODULE_1_chartist__["Interpolation"].cardinal({
                tension: 0
            }),
            low: 0,
            high: 50,
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0 },
        };
        var dailySalesChart = new __WEBPACK_IMPORTED_MODULE_1_chartist__["Line"]('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);
        this.startAnimationForLineChart(dailySalesChart);
        /* ----------==========     Completed Tasks Chart initialization    ==========---------- */
        var dataCompletedTasksChart = {
            labels: ['12am', '3pm', '6pm', '9pm', '12pm', '3am', '6am', '9am'],
            series: [
                [230, 750, 450, 300, 280, 240, 200, 190]
            ]
        };
        var optionsCompletedTasksChart = {
            lineSmooth: __WEBPACK_IMPORTED_MODULE_1_chartist__["Interpolation"].cardinal({
                tension: 0
            }),
            low: 0,
            high: 1000,
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0 }
        };
        var completedTasksChart = new __WEBPACK_IMPORTED_MODULE_1_chartist__["Line"]('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);
        // start animation for the Completed Tasks Chart - Line Chart
        this.startAnimationForLineChart(completedTasksChart);
        /* ----------==========     Emails Subscription Chart initialization    ==========---------- */
        var dataEmailsSubscriptionChart = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [
                [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]
            ]
        };
        var optionsEmailsSubscriptionChart = {
            axisX: {
                showGrid: false
            },
            low: 0,
            high: 1000,
            chartPadding: { top: 0, right: 5, bottom: 0, left: 0 }
        };
        var responsiveOptions = [
            ['screen and (max-width: 640px)', {
                    seriesBarDistance: 5,
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
        ];
        var emailsSubscriptionChart = new __WEBPACK_IMPORTED_MODULE_1_chartist__["Bar"]('#emailsSubscriptionChart', dataEmailsSubscriptionChart, optionsEmailsSubscriptionChart, responsiveOptions);
        //start animation for the Emails Subscription Chart
        this.startAnimationForBarChart(emailsSubscriptionChart);
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-dashboard',
            template: __webpack_require__("../../../../../src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "../../../../../src/app/error/error404/error404.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"main-content\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <div class=\"card\">\r\n                           \r\n                   <div class=\"card-content\">\r\n                    <div><img src=\"../../assets/404.jpg\" ></div>\r\n      </div>\r\n\r\n                \r\n            </div>\r\n          </div>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "../../../../../src/app/error/error404/error404.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/error/error404/error404.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Error404Component; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Error404Component = (function () {
    function Error404Component() {
    }
    Error404Component.prototype.ngOnInit = function () {
    };
    Error404Component = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-error404',
            template: __webpack_require__("../../../../../src/app/error/error404/error404.component.html"),
            styles: [__webpack_require__("../../../../../src/app/error/error404/error404.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], Error404Component);
    return Error404Component;
}());



/***/ }),

/***/ "../../../../../src/app/main/analysis/analysis.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header\" data-background-color=\"appraisal\">\n                        <h4 class=\"title\">Operating expenses - analysis</h4>\n                   </div>     \n                   <div class=\"row\">\n\n                        <div class=\"col-md-1\"></div>   \n                        \n                        <div class=\"col-md-10\">           \n                      <div class=\"card-content table-responsive\">\n                         \n                         \n                        <table class=\"table table-hover\">\n                          <thead class=\"text-appraisal\">\n                            <tr style=\"text-transform: capitalize;\">\n                                <th></th>\n                                <th></th>\n                                <th>Absolute <br> Amount</th>\n                                <th>$/SF</th>\n                                <th>% of<br>Total<br>Expenses</th>\n                                <th>% of<br>Effective<br>Gross Income</th>\n                                <th></th>\n                                <th></th>\n                                <th>Med</th>\n                                <th>Low</th>\n                                <th>High</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr>\n                              <td >Administration</td>\n                              <td ></td>\n                              <td > {{ operatingExpense.administration | currency }} </td>\n                              <td > {{ operatingExpense.administration / operatingExpense.surface | currency }} </td>\n                              <td > {{ operatingExpense.administration / operatingExpense.totalAllExpenses  | percent: '2.1-2' }}</td>\n                              <td >{{ operatingExpense.administration / operatingExpense.effectiveGrossIncome | percent: '2.1-2' }}</td>                   \n                              <td></td>\n                              <td><span  class=\"btn BelowRange\" style=\"width: 80%\" >Below Range</span> </td>\n                              <td class=\"text\"> </td>\n                              <td class=\"text\"> </td>\n                              <td class=\"text\"> </td>\n                            </tr> \n                            \n                            <tr>\n                                <td >Utilities</td>\n                                <td ></td>\n                                <td > {{ operatingExpense.utilities | currency }} </td>\n                                <td > {{ operatingExpense.utilities  / operatingExpense.surface | currency }}</td>\n                                <td >{{ operatingExpense.utilities  / operatingExpense.totalAllExpenses | percent: '2.1-2' }}</td>\n                                <td >{{ operatingExpense.utilities / operatingExpense.effectiveGrossIncome | percent: '2.1-2' }}</td>                  \n                                <td></td>\n                                <td><span  class=\"btn BelowRange\" style=\"width: 80%\" >Below Range</span> </td>\n                                <td class=\"text\"> 12</td>\n                                <td class=\"text\"> 22</td>\n                                <td class=\"text\"> 22</td>\n                              </tr> \n                            \n                              <tr>\n                                <td >Payroll</td>\n                                <td ></td>\n                                <td > {{ operatingExpense.payroll | currency }}</td>\n                                <td > {{ operatingExpense.payroll  / operatingExpense.surface  | currency}}</td>\n                                <td >{{ operatingExpense.payroll / operatingExpense.totalAllExpenses | percent: '2.1-2' }}</td>\n                                <td >{{ operatingExpense.payroll / operatingExpense.effectiveGrossIncome | percent: '2.1-2' }}</td>                 \n                                <td></td>\n                                <td><span  class=\"btn Median\" style=\"width: 80%\" >Below Range</span> </td>\n                                <td class=\"text\"> 155</td>\n                                <td class=\"text\"> 22</td>\n                                <td class=\"text\"> 22</td>\n                              </tr> \n                            \n                              <tr>\n                                <td >Repairs and Maintenance</td>\n                                <td ></td>\n                                <td > {{ operatingExpense.repairsMaintenance | currency }} </td>\n                                <td > {{ operatingExpense.repairsMaintenance  / operatingExpense.surface | currency }}</td>\n                                <td > {{ operatingExpense.repairsMaintenance / operatingExpense.totalAllExpenses | percent: '2.1-2'}}</td>\n                                <td >{{ operatingExpense.replacementReserves / operatingExpense.effectiveGrossIncome | percent: '2.1-2' }}</td>                    \n                                <td></td>\n                                <td><span  class=\"btn Median\" style=\"width: 80%\" >Median</span> </td>\n                                <td class=\"text\">110</td>\n                                <td class=\"text\">22</td>\n                                <td class=\"text\">2</td>\n                              </tr> \n                            \n                              <tr>\n                                <td >Cleaning & Janitorial</td>\n                                <td ></td>\n                                <td > {{operatingExpense.cleaningJanitorial | currency}} </td>\n                                <td > {{operatingExpense.cleaningJanitorial / operatingExpense.surface  | currency}} </td>\n                                <td >{{ operatingExpense.cleaningJanitorial / operatingExpense.totalAllExpenses | percent: '2.1-2' }}</td>\n                                <td >{{ operatingExpense.cleaningJanitorial / operatingExpense.effectiveGrossIncome | percent: '2.1-2' }}</td>                   \n                                <td></td>\n                                <td><span  class=\"btn AboveMedian\" style=\"width: 80%\" >Above Median</span> </td>\n                                <td class=\"text\"> 11</td>\n                                <td class=\"text\"> 22</td>\n                                <td class=\"text\"> 22</td>\n                              </tr> \n                            \n                              <tr>\n                                <td >CAM</td>\n                                <td ></td>\n                                <td > {{ operatingExpense.cam | currency }}</td>\n                                <td > {{ operatingExpense.cam   / operatingExpense.surface | currency}}</td>\n                                <td > {{ operatingExpense.cam  / operatingExpense.totalAllExpenses | percent: '2.1-2' }}</td>\n                                <td >{{ operatingExpense.cam / operatingExpense.effectiveGrossIncome | percent: '2.1-2' }}</td>                     \n                                <td></td>\n                                <td><span  class=\"btn BelowMedian\" style=\"width: 80%\" >Below Median</span> </td>\n                                <td class=\"text\">1158</td>\n                                <td class=\"text\">22</td>\n                                <td class=\"text\">22</td>\n                              </tr> \n                            \n                              <tr>\n                                <td >Real Estate Taxes</td>\n                                <td ></td>\n                                <td > {{ operatingExpense.realEstateTaxes | currency }}</td>\n                                <td > {{ operatingExpense.realEstateTaxes / operatingExpense.surface  | currency }}</td>\n                                <td >{{ operatingExpense.realEstateTaxes / operatingExpense.totalAllExpenses | percent: '2.1-2' }}</td>\n                                <td >{{ operatingExpense.realEstateTaxes / operatingExpense.effectiveGrossIncome | percent: '2.1-2' }}</td>                \n                                <td></td>\n                                <td><span  class=\"btn AboveMedian\" style=\"width: 80%\" >Above Median</span> </td>\n                                <td class=\"text\"> 65</td>\n                                <td class=\"text\"> 22</td>\n                                <td class=\"text\"> 22</td>\n                              </tr> \n                            \n                              <tr>\n                                <td >Insurance</td>\n                                <td ></td>\n                                <td > {{ operatingExpense.insurance | currency}}</td>\n                                <td > {{ operatingExpense.insurance / operatingExpense.surface  | currency}}</td>\n                                <td >{{ operatingExpense.insurance / operatingExpense.totalAllExpenses | percent: '2.1-2' }}</td>\n                                <td >{{ operatingExpense.insurance / operatingExpense.effectiveGrossIncome | percent: '2.1-2' }}</td>                      \n                                <td></td>\n                                <td><span  class=\"btn Median\" style=\"width: 80%\" >Median</span> </td>\n                                <td class=\"text\"> 65</td>\n                                <td class=\"text\"> 22</td>\n                                <td class=\"text\"> 22</td>\n                              </tr> \n                            \n                              <tr>\n                                <td >Replacement Reserves</td>\n                                <td ></td>\n                                <td >{{ operatingExpense.replacementReserves | currency }}</td>\n                                <td >{{ operatingExpense.replacementReserves  / operatingExpense.surface  | currency}}</td>\n                                <td >{{ operatingExpense.replacementReserves / operatingExpense.totalAllExpenses | percent: '2.1-2' }}</td>\n                                <td >{{ operatingExpense.replacementReserves / operatingExpense.effectiveGrossIncome | percent: '2.1-2' }}</td>                      \n                                <td></td>\n                                <td><span  class=\"btn BelowRange\" style=\"width: 80%\" >Below Range</span> </td>\n                                <td class=\"text\"> 65</td>\n                                <td class=\"text\"> 22</td>\n                                <td class=\"text\"> 22</td>\n                              </tr> \n                                       \n                            <tr >\n                                <td class=\"text-danger\" > <h4>TOTAL : </h4></td>\n                                <td></td>\n                                <td class=\"text-danger\">{{ ConvertToInt(operatingExpense.administration) + ConvertToInt(operatingExpense.utilities) +        ConvertToInt(operatingExpense.payroll) + ConvertToInt(operatingExpense.repairsMaintenance) + ConvertToInt(operatingExpense.cleaningJanitorial) + ConvertToInt(operatingExpense.cam) + ConvertToInt(operatingExpense.realEstateTaxes) + ConvertToInt(operatingExpense.insurance) +\n                                  ConvertToInt(operatingExpense.replacementReserves) | number }}</td>\n                                <td class=\"text-danger\">{{ ConvertToInt(operatingExpense.administration) + ConvertToInt(operatingExpense.utilities) +        ConvertToInt(operatingExpense.payroll) + ConvertToInt(operatingExpense.repairsMaintenance) + ConvertToInt(operatingExpense.cleaningJanitorial) + ConvertToInt(operatingExpense.cam) + ConvertToInt(operatingExpense.realEstateTaxes) + ConvertToInt(operatingExpense.insurance) +\n                                  ConvertToInt(operatingExpense.replacementReserves) / operatingExpense.surface  | currency }} </td>\n                                <td class=\"text-danger\">{{ ConvertToInt(operatingExpense.administration) + ConvertToInt(operatingExpense.utilities) +        ConvertToInt(operatingExpense.payroll) + ConvertToInt(operatingExpense.repairsMaintenance) + ConvertToInt(operatingExpense.cleaningJanitorial) + ConvertToInt(operatingExpense.cam) + ConvertToInt(operatingExpense.realEstateTaxes) + ConvertToInt(operatingExpense.insurance) +\n                                  ConvertToInt(operatingExpense.replacementReserves) / operatingExpense.totalAllExpenses | percent: '2.1-2'  }}</td>\n                                <td class=\"text-danger\">{{ ConvertToInt(operatingExpense.administration) + ConvertToInt(operatingExpense.utilities) +        ConvertToInt(operatingExpense.payroll) + ConvertToInt(operatingExpense.repairsMaintenance) + ConvertToInt(operatingExpense.cleaningJanitorial) + ConvertToInt(operatingExpense.cam) + ConvertToInt(operatingExpense.realEstateTaxes) + ConvertToInt(operatingExpense.insurance) +\n                                  ConvertToInt(operatingExpense.replacementReserves) / operatingExpense.effectiveGrossIncome | percent: '2.1-2' }}</td>\n                                <td></td>\n                                <td></td>\n                                <td class=\"text-danger\">1,388</td>\n                                <td class=\"text-danger\">56,233</td>\n                                <td class=\"text-danger\">5,61</td>\n\n                              </tr> \n                       </tbody>\n                       </table>\n\n                    <button type=\"submit\"  class=\"btn btn-appraisal pull-right\" (click)=\"test()\">Export PDF</button>\n                    </div>\n\n                </div>\n                <div class=\"col-md-1\"></div>\n                    </div>\n\n                </div>\n            </div>\n          </div>\n    </div>\n  </div>"

/***/ }),

/***/ "../../../../../src/app/main/analysis/analysis.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".BelowRange {\n  background: #00b0f0; }\n\n.Median {\n  background: #86bc64; }\n\n.AboveRange {\n  background: #ff0000; }\n\n.BelowMedian {\n  background-color: #6f3; }\n\n.AboveMedian {\n  background-color: #f5ce68; }\n\n.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {\n  padding: 0px 0px !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/analysis/analysis.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnalysisComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_services_data_services_operatingExpense_data_service__ = __webpack_require__("../../../../../src/app/services/data-services/operatingExpense-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_model_OperatingExpense__ = __webpack_require__("../../../../../src/app/model/OperatingExpense.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_globals_service__ = __webpack_require__("../../../../../src/app/services/globals.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_model_propertyCharacteristic__ = __webpack_require__("../../../../../src/app/model/propertyCharacteristic.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AnalysisComponent = (function () {
    function AnalysisComponent(globalsService, operatingExpenseDataService, router) {
        this.globalsService = globalsService;
        this.operatingExpenseDataService = operatingExpenseDataService;
        this.router = router;
        this.operatingExpense = new __WEBPACK_IMPORTED_MODULE_2_app_model_OperatingExpense__["a" /* OperatingExpense */]();
        this.propertyCharacteristic = new __WEBPACK_IMPORTED_MODULE_4_app_model_propertyCharacteristic__["a" /* PropertyCharacteristic */]();
    }
    AnalysisComponent.prototype.ngOnInit = function () {
        this.propertyCharacteristic = this.globalsService.getPropertyCharacteristic();
        this.operatingExpense = this.globalsService.getOperatingExpense();
    };
    AnalysisComponent.prototype.ConvertToInt = function (currentPage) {
        return parseInt(currentPage);
    };
    AnalysisComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-analysis',
            template: __webpack_require__("../../../../../src/app/main/analysis/analysis.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/analysis/analysis.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_app_services_globals_service__["a" /* GlobalsService */], __WEBPACK_IMPORTED_MODULE_1_app_services_data_services_operatingExpense_data_service__["a" /* OperatingExpenseDataService */], __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* Router */]])
    ], AnalysisComponent);
    return AnalysisComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/connexion/connexion.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"main-content\">\n  <div class=\"container-fluid\">\n      <div class=\"row\">\n          <div class=\"col-md-4\"></div>\n          <div class=\"col-md-4\">\n              <div class=\"card\">\n                  <div class=\"card-header\" data-background-color=\"appraisal\">\n                    <div class=\"nav-tabs-navigation\">\n                      <div class=\"nav-tabs-wrapper\">\n                          <ul class=\"nav nav-tabs\" data-tabs=\"tabs\">\n                              <li class=\"active\">\n                                  <a href=\"#profile\" data-toggle=\"tab\">\n                                    login\n                                  <div class=\"ripple-container\"></div></a>\n                              </li>\n                              <li class=\"\">\n                                  <a href=\"#msg\" data-toggle=\"tab\">\n                                    singup\n                                  <div class=\"ripple-container\"></div></a>\n                              </li>\n                 \n                        \n                          </ul>\n                      </div>\n                  </div>\n                 </div>                \n                 <div class=\"card-content\">\n\n                    <div class=\"tab-content\">\n                        <div class=\"tab-pane active\" id=\"profile\">\n                            <table class=\"table\">\n                                <tbody>\n                                    <app-login></app-login>\n                                </tbody>\n                            </table>\n                        </div>\n                        <div class=\"tab-pane\" id=\"msg\">\n                            <table class=\"table\">\n                                <tbody>\n                                    <app-signup></app-signup>\n                                </tbody>\n                            </table>\n                        </div> </div>\n     \n                \n              </div>\n          </div>\n          <div class=\"col-md-4\"></div>\n        </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/main/connexion/connexion.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".mat-tab-body-content {\n  height: 0 !important;\n  overflow: 0 !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/connexion/connexion.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnexionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_services_auth_navbar_service__ = __webpack_require__("../../../../../src/app/services/auth/navbar.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ConnexionComponent = (function () {
    function ConnexionComponent(nav) {
        this.nav = nav;
    }
    ConnexionComponent.prototype.ngOnInit = function () {
        this.nav.hide();
    };
    ConnexionComponent.prototype.ngOnDestroy = function () {
        this.nav.show();
    };
    ConnexionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-connexion',
            template: __webpack_require__("../../../../../src/app/main/connexion/connexion.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/connexion/connexion.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_app_services_auth_navbar_service__["a" /* NavbarService */]])
    ], ConnexionComponent);
    return ConnexionComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/connexion/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n<div class=\"row\">\n  <div class=\"col-md-1\">\n\n  </div>\n  <div class=\"col-md-10\">\n      <form>\n                    \n          <div class=\"row\">\n          <div class=\"col-md-12\">\n            <mat-form-field style=\" width: 100%;\">\n                  <input matInput placeholder=\"Enter your email\" [(ngModel)]=\"email\" name=\"email\" required>\n                     </mat-form-field>\n          </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"col-md-12\">\n             <mat-form-field style=\" width: 100%;\">\n                    <input matInput placeholder=\"Enter your password\" [type]=\"hide ? 'password' : 'text'\" [(ngModel)]=\"password\" name=\"password\">\n                    <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility' : 'visibility_off'}} </mat-icon>\n              </mat-form-field>\n                <label href=\"#\"><a class=\"text-info\">Forgot your password?</a></label>\n                <button class=\"btn btn-appraisal pull-right\" (click)=\"login()\" >Login</button>\n                <br>\n      \n              \n            </div>\n          </div>\n                  <div class=\"clearfix\"><br>\n\n                </div>\n            </form>\n  </div>\n\n  <div class=\"col-md-1\">\n    \n    </div>\n</div>\n          \n\n  </div>"

/***/ }),

/***/ "../../../../../src/app/main/connexion/login/login.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "a:hover {\n  color: rgba(231, 231, 12, 0.918); }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/connexion/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_af_service__ = __webpack_require__("../../../../../src/app/services/auth/af.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = (function () {
    function LoginComponent(afService, router) {
        this.afService = afService;
        this.router = router;
        this.hide = true;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.afService.login(this.email, this.password).then(function (res) {
            console.log('ca marche');
            _this.router.navigate(['/home']);
        })
            .catch(function (err) {
            console.log('ca marche pas');
        });
    };
    LoginComponent.prototype.loginG = function () {
        this.afService.loginWithGoogle();
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/main/connexion/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/connexion/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_af_service__["a" /* AfService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/connexion/signup/signup.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n<div class=\"row\">\n    <div class=\"col-md-1\"></div>\n    <div class=\"col-md-10\">\n      <form  #formsearch=\"ngForm\">\n\n        <div class=\"row\">\n          <div class=\"col-md-12\">\n           <mat-form-field class=\"width\">\n                  <input matInput placeholder=\"First Name\" [(ngModel)]=\"user.firstName\" name=\"user.firstName\">\n            </mat-form-field>\n          </div>\n        </div>\n          <div class=\"row\">\n            <div class=\"col-md-12\">\n            <mat-form-field class=\"width\">\n                    <input matInput placeholder=\"Last Name\" [(ngModel)]=\"user.lastName\" name=\"user.lastName\">\n              </mat-form-field>\n            </div>\n          </div>\n          <div class=\"row\">\n              <div class=\"col-md-12\">\n                  <mat-form-field class=\"width\">\n                      <mat-select placeholder=\"I’m a ...\"  >\n                        <mat-option *ngFor=\"let job of jobs\" [value]=\"job.value\">\n                          {{ job.viewValue }}\n                        </mat-option>\n                      </mat-select>\n                    </mat-form-field>\n              </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                  <mat-form-field class=\"width\">\n                        <input matInput placeholder=\"Enter your email\"  [(ngModel)]=\"user.email\" name=\"user.email\" required>\n                  </mat-form-field>\n                </div>\n                </div>\n    \n                   <div class=\"row\">\n                          <div class=\"col-md-12\">\n                           <mat-form-field style=\" width: 100%;\">\n                                  <input matInput placeholder=\"Password\" [type]=\"hide ? 'password' : 'text'\" [(ngModel)]=\"password\" name=\"password\" >\n                                  <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility' : 'visibility_off'}} </mat-icon>\n                            </mat-form-field>\n                          </div>\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col-md-12\">\n                             <mat-form-field style=\" width: 100%;\">\n                                    <input matInput placeholder=\"Password confirmation\" [type]=\"hide ? 'password' : 'text'\">\n                                    <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility' : 'visibility_off'}} </mat-icon>\n                              </mat-form-field>\n                              <button type=\"submit\" class=\"btn btn-appraisal pull-right\" (click)=\"onSinup()\" >Sign up</button>\n                            </div>\n                        </div>\n    \n      \n          <div class=\"clearfix\"></div>\n      </form>\n    </div>\n    <div class=\"col-md-1\"></div>\n</div>\n  \n\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/connexion/signup/signup.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".width {\n  width: 100%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/connexion/signup/signup.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_af_service__ = __webpack_require__("../../../../../src/app/services/auth/af.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_User__ = __webpack_require__("../../../../../src/app/model/User.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_data_services_user_data_service__ = __webpack_require__("../../../../../src/app/services/data-services/user-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SignupComponent = (function () {
    function SignupComponent(authService, userDataService, router) {
        this.authService = authService;
        this.userDataService = userDataService;
        this.router = router;
        // firstName:string;
        // lastName:string;
        // job: string;
        this.user = new __WEBPACK_IMPORTED_MODULE_2__model_User__["a" /* User */]();
        this.hide = true;
        this.jobs = [
            { viewValue: 'Appraiser', value: 'appraiser' },
            { viewValue: 'Property Manager', value: 'propertyManager' },
            { viewValue: 'Building Owner', value: 'buildingOwner' },
            { viewValue: 'Tenant', value: 'tenant' },
            { viewValue: 'Tax Assessor', value: 'taxAssessor' },
            { viewValue: 'Mortgage Broker', value: 'mortgageBroker' },
            { viewValue: 'Real Estate Broker', value: 'realEstateBroker' },
            { viewValue: 'Real Estate Consultant', value: 'realEstateConsultant' },
            { viewValue: 'Developer', value: 'developer' },
        ];
    }
    SignupComponent.prototype.ngOnInit = function () {
    };
    SignupComponent.prototype.onSinup = function () {
        this.authService.register(this.user.email, this.password);
        this.userDataService.creatUser(this.user).subscribe(function (res) { return res; });
        this.router.navigate(['/']);
    };
    SignupComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-signup',
            template: __webpack_require__("../../../../../src/app/main/connexion/signup/signup.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/connexion/signup/signup.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_auth_af_service__["a" /* AfService */], __WEBPACK_IMPORTED_MODULE_3__services_data_services_user_data_service__["a" /* UserDataService */], __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/langing-page/langing-page.component.html":
/***/ (function(module, exports) {

module.exports = "\n  <ul class=\"nav nav-pills nav-justified\">\n      <li class=\"nav-item\" [routerLinkActiveOptions]=\"{exact:true}\"  routerLinkActive=\"zz\">\n        <a class=\"nav-link\"  [routerLink]=\"['new-analysis']\" >New Analysis</a>\n      </li>\n      <li class=\"nav-item\" [routerLinkActiveOptions]=\"{exact:true}\"  routerLinkActive=\"zz\" >\n        <a class=\"nav-link\" [routerLink]=\"['new-property']\">Property Characteristics</a>\n      </li>\n      <li class=\"nav-item\" [routerLinkActiveOptions]=\"{exact:true}\"  routerLinkActive=\"zz\">\n          <a class=\"nav-link\" [routerLink]=\"['new-data-entry']\" >Operating Expenses Data Entry</a>\n        </li>\n      <li class=\"nav-item\" [routerLinkActiveOptions]=\"{exact:true}\"  routerLinkActive=\"zz\">\n        <a class=\"nav-link\" [routerLink]=\"['analysis']\" >Operating Expenses - Analysis</a>\n      </li>\n    </ul>\n    <br>\n    <router-outlet></router-outlet>\n\n\n<!-- <mat-horizontal-stepper [linear]=\"isLinear\" #stepper=\"matHorizontalStepper\">\n  <mat-step [stepControl]=\"firstFormGroup\" style=\"background-color: red\">\n    <form>\n      <ng-template matStepLabel color=\"warn\">New Analysis</ng-template>\n      \n      <app-new-analysis #newAnalysisComponent></app-new-analysis>\n     \n    </form>\n  </mat-step>\n  \n  <mat-step [stepControl]=\"secondFormGroup\">\n    <form>\n      <ng-template matStepLabel>Property Characteristics</ng-template>\n<app-new-property #newPropertyComponent></app-new-property>\n \n    </form>\n  </mat-step>\n  <mat-step [stepControl]=\"thirdFormGroup\">\n      <form >\n        <ng-template matStepLabel>Operating Expenses Data Entry</ng-template>\n  <app-new-data-entry #newDataEntryComponent></app-new-data-entry>\n        <div>\n          <button mat-button matStepperPrevious>Back</button>\n          <button mat-button (click)=\"onOperatingExpensesDataEntry(stepper)\" class=\"btn btn-appraisal\">Save</button>\n          <button mat-button class=\"btn btn-appraisal\" (click)=\"openDialog()\" >Add another year</button>\n        </div>\n      </form>\n    </mat-step>\n   \n    <mat-step [stepControl]=\"fourthFormGroup\">\n        <form >\n          <ng-template matStepLabel> Operating Expenses - Analysis</ng-template>\n    <app-analysis #analysisComponent></app-analysis>\n          <div>\n            <button mat-button matStepperPrevious>Back</button>\n            <button mat-button matStepperNext matStepperNext class=\"btn btn-appraisal\">Export PDF</button>\n          </div>\n        </form>\n      </mat-step>\n\n  <mat-step>\n    <ng-template matStepLabel>Done</ng-template>\n    You are now done.\n    <div>\n      <button mat-button matStepperPrevious>Back</button>\n      <button mat-button (click)=\"stepper.reset()\" matStepperNext class=\"btn btn-appraisal\">Reset</button>\n    </div>\n  </mat-step>\n</mat-horizontal-stepper> -->"

/***/ }),

/***/ "../../../../../src/app/main/langing-page/langing-page.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".zz {\n  color: #e45641 !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/langing-page/langing-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LangingPageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__popup_data_year_data_year_component__ = __webpack_require__("../../../../../src/app/main/popup/data-year/data-year.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_main_new_property_new_property_component__ = __webpack_require__("../../../../../src/app/main/new-property/new-property.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_model_propertyCharacteristic__ = __webpack_require__("../../../../../src/app/model/propertyCharacteristic.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_model_OperatingExpense__ = __webpack_require__("../../../../../src/app/model/OperatingExpense.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_app_main_new_analysis_new_analysis_component__ = __webpack_require__("../../../../../src/app/main/new-analysis/new-analysis.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_app_main_new_data_entry_new_data_entry_component__ = __webpack_require__("../../../../../src/app/main/new-data-entry/new-data-entry.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_data_services_operatingExpense_data_service__ = __webpack_require__("../../../../../src/app/services/data-services/operatingExpense-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_data_services_propertyCharacteristic_data_service__ = __webpack_require__("../../../../../src/app/services/data-services/propertyCharacteristic-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_app_main_analysis_analysis_component__ = __webpack_require__("../../../../../src/app/main/analysis/analysis.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var LangingPageComponent = (function () {
    function LangingPageComponent(dialog, operatingExpenseDataService, propertyCharacteristicDataService) {
        this.dialog = dialog;
        this.operatingExpenseDataService = operatingExpenseDataService;
        this.propertyCharacteristicDataService = propertyCharacteristicDataService;
        this.operatingExpense = new __WEBPACK_IMPORTED_MODULE_5_app_model_OperatingExpense__["a" /* OperatingExpense */];
        this.propertyCharacteristic = new __WEBPACK_IMPORTED_MODULE_4_app_model_propertyCharacteristic__["a" /* PropertyCharacteristic */];
    }
    LangingPageComponent.prototype.ngOnInit = function () {
    };
    LangingPageComponent.prototype.onPropertyCharacteristics = function (stepper) {
        this.propertyCharacteristic.propertySubType = this.newPropertyComponent.propertyCharacteristic.propertySubType;
        this.propertyCharacteristic.address = this.newPropertyComponent.propertyCharacteristic.address;
        this.propertyCharacteristic.zipCode = this.newPropertyComponent.propertyCharacteristic.zipCode;
        this.propertyCharacteristic.city = this.newPropertyComponent.propertyCharacteristic.city;
        this.propertyCharacteristic.msa = this.newPropertyComponent.propertyCharacteristic.msa;
        this.propertyCharacteristic.gba = this.newPropertyComponent.propertyCharacteristic.gba;
        this.propertyCharacteristic.nra = this.newPropertyComponent.propertyCharacteristic.nra;
        this.propertyCharacteristic.numberUnit = this.newPropertyComponent.propertyCharacteristic.numberUnit;
        this.propertyCharacteristic.averageUnitSize = this.newPropertyComponent.propertyCharacteristic.averageUnitSize;
        this.propertyCharacteristic.rentableArea = this.newPropertyComponent.propertyCharacteristic.rentableArea;
        this.propertyCharacteristic.numberTenants = this.newPropertyComponent.propertyCharacteristic.numberTenants;
        this.propertyCharacteristic.yoc = this.newPropertyComponent.propertyCharacteristic.yoc;
        this.propertyCharacteristic.investmentClass = this.newPropertyComponent.propertyCharacteristic.investmentClass;
        this.propertyCharacteristic.yearBuilt = this.newPropertyComponent.propertyCharacteristic.yearBuilt;
        this.propertyCharacteristic.yearMostRenovation = this.newPropertyComponent.propertyCharacteristic.yearMostRenovation;
        this.propertyCharacteristic.surface = (this.propertyCharacteristic.numberUnit * this.propertyCharacteristic.averageUnitSize);
        console.log(this.propertyCharacteristic);
        console.log(this.operatingExpense);
        stepper.next();
    };
    LangingPageComponent.prototype.onOperatingExpensesDataEntry = function (stepper) {
        this.operatingExpense.effectiveGrossIncome = this.newDataEntryComponent.operatingExpense.effectiveGrossIncome;
        this.operatingExpense.administration = this.newDataEntryComponent.operatingExpense.administration;
        this.operatingExpense.utilities = this.newDataEntryComponent.operatingExpense.utilities;
        this.operatingExpense.payroll = this.newDataEntryComponent.operatingExpense.payroll;
        this.operatingExpense.repairsMaintenance = this.newDataEntryComponent.operatingExpense.repairsMaintenance;
        this.operatingExpense.cleaningJanitorial = this.newDataEntryComponent.operatingExpense.cleaningJanitorial;
        this.operatingExpense.cam = this.newDataEntryComponent.operatingExpense.cam;
        this.operatingExpense.realEstateTaxes = this.newDataEntryComponent.operatingExpense.realEstateTaxes;
        this.operatingExpense.insurance = this.newDataEntryComponent.operatingExpense.insurance;
        this.operatingExpense.replacementReserves = this.newDataEntryComponent.operatingExpense.replacementReserves;
        this.operatingExpense.totalAllExpenses = this.newDataEntryComponent.operatingExpense.totalAllExpenses;
        // this.operatingExpense.totalAllExpenses = (this.operatingExpense.replacementReserves + this.operatingExpense.insurance + this.operatingExpense.realEstateTaxes + this.operatingExpense.cam +  this.operatingExpense.cleaningJanitorial +  this.operatingExpense.repairsMaintenance +  this.operatingExpense.payroll + this.operatingExpense.utilities + this.operatingExpense.administration + this.operatingExpense.effectiveGrossIncome )
        this.operatingExpense.netOperatingIncome = this.newDataEntryComponent.operatingExpense.netOperatingIncome;
        this.analysisComponent.operatingExpense.effectiveGrossIncome = this.newDataEntryComponent.operatingExpense.effectiveGrossIncome;
        this.analysisComponent.operatingExpense.administration = this.newDataEntryComponent.operatingExpense.administration;
        this.analysisComponent.operatingExpense.utilities = this.newDataEntryComponent.operatingExpense.utilities;
        this.analysisComponent.operatingExpense.payroll = this.newDataEntryComponent.operatingExpense.payroll;
        this.analysisComponent.operatingExpense.repairsMaintenance = this.newDataEntryComponent.operatingExpense.repairsMaintenance;
        this.analysisComponent.operatingExpense.cleaningJanitorial = this.newDataEntryComponent.operatingExpense.cleaningJanitorial;
        this.analysisComponent.operatingExpense.cam = this.newDataEntryComponent.operatingExpense.cam;
        this.analysisComponent.operatingExpense.realEstateTaxes = this.newDataEntryComponent.operatingExpense.realEstateTaxes;
        this.analysisComponent.operatingExpense.insurance = this.newDataEntryComponent.operatingExpense.insurance;
        this.analysisComponent.operatingExpense.replacementReserves = this.newDataEntryComponent.operatingExpense.replacementReserves;
        this.analysisComponent.total = (this.analysisComponent.operatingExpense.administration + this.analysisComponent.operatingExpense.utilities +
            this.analysisComponent.operatingExpense.payroll +
            this.analysisComponent.operatingExpense.repairsMaintenance + this.operatingExpense.cleaningJanitorial +
            this.analysisComponent.operatingExpense.cam +
            this.analysisComponent.operatingExpense.realEstateTaxes +
            this.analysisComponent.operatingExpense.insurance +
            this.analysisComponent.operatingExpense.replacementReserves);
        this.analysisComponent.operatingExpense.totalAllExpenses = this.operatingExpense.totalAllExpenses;
        this.analysisComponent.operatingExpense.netOperatingIncome = this.newDataEntryComponent.operatingExpense.netOperatingIncome;
        this.onSave();
        stepper.next();
    };
    LangingPageComponent.prototype.onTest = function () {
        console.log(this.analysisComponent.total);
    };
    LangingPageComponent.prototype.onSave = function () {
        this.operatingExpenseDataService.creatOperatingExpense(this.operatingExpense)
            .subscribe(function (res) { return console.log("ca marche pour operatingExpenseDataService "); }, function (error) { return console.log("ca marche pas "); });
        this.propertyCharacteristicDataService.creatPropertyCharacteristic(this.propertyCharacteristic).subscribe(function (res) { return console.log("ca marche pour propertyCharacteristicDataService "); }, function (error) { return console.log("ca marche pas propertyCharacteristicDataService "); });
    };
    LangingPageComponent.prototype.openDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_2__popup_data_year_data_year_component__["a" /* DataYearComponent */], {
            width: '700px',
            data: {}
        });
        dialogRef.afterClosed().subscribe(function (res) {
            console.log(res);
            _this.operatingExpenseDataService.creatOperatingExpense(_this.operatingExpense)
                .subscribe(function (res) { return console.log("ca marche pour operatingExpenseDataService "); }, function (error) { return console.log("ca marche pas "); });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])("newPropertyComponent"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_app_main_new_property_new_property_component__["a" /* NewPropertyComponent */])
    ], LangingPageComponent.prototype, "newPropertyComponent", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])("newAnalysisComponent"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_app_main_new_analysis_new_analysis_component__["a" /* NewAnalysisComponent */])
    ], LangingPageComponent.prototype, "newAnalysisComponent", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])("newDataEntryComponent"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_7_app_main_new_data_entry_new_data_entry_component__["a" /* NewDataEntryComponent */])
    ], LangingPageComponent.prototype, "newDataEntryComponent", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])("analysisComponent"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_10_app_main_analysis_analysis_component__["a" /* AnalysisComponent */])
    ], LangingPageComponent.prototype, "analysisComponent", void 0);
    LangingPageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-langing-page',
            template: __webpack_require__("../../../../../src/app/main/langing-page/langing-page.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/langing-page/langing-page.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatDialog */], __WEBPACK_IMPORTED_MODULE_8__services_data_services_operatingExpense_data_service__["a" /* OperatingExpenseDataService */], __WEBPACK_IMPORTED_MODULE_9__services_data_services_propertyCharacteristic_data_service__["a" /* PropertyCharacteristicDataService */]])
    ], LangingPageComponent);
    return LangingPageComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/list-user/list-user.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header\" data-background-color=\"appraisal\">\n                        <h4 class=\"title\">Users List</h4>\n                   </div>     \n                   <div class=\"row\">\n\n                        <div class=\"col-md-1\"></div>   \n                        \n                        <div class=\"col-md-10\">           \n                      <div class=\"card-content table-responsive\">\n                         \n                         \n                        <table class=\"table table-hover\">\n                          <thead class=\"text-appraisal\">\n                       <tr>  \n                           <th>First Name</th>\n                           <th>Last name</th>\n                           <th>im</th>\n                           <th>email</th>\n                       </tr>\n                        </thead>\n                        <tbody>\n                                <tr *ngFor=\"let user of users\" > \n                                        <td>{{user.firstName}}</td>\n                                        <td>{{user.lastName}}</td>\n                                        <td>{{user.im}}</td>\n                                        <td>{{user.email}}</td>\n                                    </tr>\n                       </tbody>\n                       </table>\n                    </div>\n                </div>\n                <div class=\"col-md-1\"></div>\n                    </div>\n                </div>\n            </div>\n          </div>\n    </div>\n  </div>"

/***/ }),

/***/ "../../../../../src/app/main/list-user/list-user.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/list-user/list-user.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListUserComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ListUserComponent = (function () {
    function ListUserComponent() {
        this.users = [
            { firstName: 'david', lastName: 'du pont', im: 'building owner', email: 'david@gmail.com' },
            { firstName: 'angel', lastName: 'delavega', im: 'tax assessor', email: 'angel@yahoo.com' },
            { firstName: 'angelina', lastName: 'jolie', im: 'tenant', email: 'buffy@gmail.com' },
            { firstName: 'momo', lastName: 'smith', im: 'appraiser', email: 'buffy@gmail.com' },
        ];
    }
    ListUserComponent.prototype.ngOnInit = function () {
    };
    ListUserComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-list-user',
            template: __webpack_require__("../../../../../src/app/main/list-user/list-user.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/list-user/list-user.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ListUserComponent);
    return ListUserComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/mydata/add-mydata/add-mydata.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  add-mydata works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/main/mydata/add-mydata/add-mydata.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/mydata/add-mydata/add-mydata.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddMydataComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AddMydataComponent = (function () {
    function AddMydataComponent() {
    }
    AddMydataComponent.prototype.ngOnInit = function () {
    };
    AddMydataComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-add-mydata',
            template: __webpack_require__("../../../../../src/app/main/mydata/add-mydata/add-mydata.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/mydata/add-mydata/add-mydata.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AddMydataComponent);
    return AddMydataComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/mydata/list-mydata/list-mydata.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  list-mydata works!\n</p>\n\n\n<ul>\n  <li *ngFor=\"let data of dataObservable | async\" >\n    \n\n    {{data.effectiveGrossIncome}}\n    {{data.administration}}\n    {{data.utilities}}\n    {{data.payroll}}\n    {{data.repairsMaintenance}}\n    {{data.cleaningJanitorial}}\n    {{data.cam}}\n    {{data.realEstateTaxes}}\n    {{data.insurance}}\n    {{data.replacementReserves}}\n    {{data.totalAllExpenses}} \n    {{data.netOperatingIncome}}\n    {{data.yearExpenseData}}\n  </li>\n</ul>"

/***/ }),

/***/ "../../../../../src/app/main/mydata/list-mydata/list-mydata.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/mydata/list-mydata/list-mydata.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListMydataComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_D_Documents_reocosts_node_modules_angularfire2_database__ = __webpack_require__("../../../../angularfire2/database/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListMydataComponent = (function () {
    function ListMydataComponent(db) {
        this.db = db;
    }
    ListMydataComponent.prototype.ngOnInit = function () {
        this.dataObservable = this.takeData('/operatingExpense');
    };
    ListMydataComponent.prototype.takeData = function (myPath) {
        return this.db.list(myPath).valueChanges();
    };
    ListMydataComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-list-mydata',
            template: __webpack_require__("../../../../../src/app/main/mydata/list-mydata/list-mydata.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/mydata/list-mydata/list-mydata.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_D_Documents_reocosts_node_modules_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], ListMydataComponent);
    return ListMydataComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/mydata/mydata.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  mydata works!\n</p>\n<app-list-mydata></app-list-mydata>"

/***/ }),

/***/ "../../../../../src/app/main/mydata/mydata.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/mydata/mydata.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MydataComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MydataComponent = (function () {
    function MydataComponent() {
    }
    MydataComponent.prototype.ngOnInit = function () {
    };
    MydataComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-mydata',
            template: __webpack_require__("../../../../../src/app/main/mydata/mydata.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/mydata/mydata.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MydataComponent);
    return MydataComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/mydata/popup-mydata/popup-mydata.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  popup-mydata works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/main/mydata/popup-mydata/popup-mydata.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/mydata/popup-mydata/popup-mydata.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupMydataComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PopupMydataComponent = (function () {
    function PopupMydataComponent() {
    }
    PopupMydataComponent.prototype.ngOnInit = function () {
    };
    PopupMydataComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-popup-mydata',
            template: __webpack_require__("../../../../../src/app/main/mydata/popup-mydata/popup-mydata.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/mydata/popup-mydata/popup-mydata.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PopupMydataComponent);
    return PopupMydataComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/myreports/list-reports/list-reports.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header\" data-background-color=\"appraisal\">\n                        <h4 class=\"title\">Reports list</h4>\n                   </div>     \n                   <div class=\"row\">\n\n                        <div class=\"col-md-1\"></div>   \n                        \n                        <div class=\"col-md-10\">           \n                      <div class=\"card-content table-responsive\">\n                         \n                         \n                        <table class=\"table table-hover\">\n                          <thead class=\"text-appraisal\">\n                       <tr>  \n                           <th>Report Date</th>\n                           <th>Property Name</th>\n                           <th>Property Type</th>\n                           <th>Zip Code</th>\n                           <th>Address</th>\n                           <th>Expenses Year</th>\n                       </tr>\n                        </thead>\n                        <tbody>\n                        <tr *ngFor=\"let report of reports\" > \n                                <td>{{report.reportDate}}</td>\n                                <td>{{report.propertyName}}</td>\n                                <td>{{report.propertyType}}</td>\n                                <td>{{report.zipCode}}</td>\n                                <td>{{report.Address}}</td>\n                                <td>{{report.expensesYear}}</td>\n                        </tr>\n                       </tbody>\n                       </table>\n                    </div>\n                </div>\n                <div class=\"col-md-1\"></div>\n                    </div>\n                </div>\n            </div>\n          </div>\n    </div>\n  </div>"

/***/ }),

/***/ "../../../../../src/app/main/myreports/list-reports/list-reports.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/myreports/list-reports/list-reports.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListReportsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ListReportsComponent = (function () {
    function ListReportsComponent() {
        this.reports = [
            { reportDate: '1/10/2018', propertyName: 'property 1', propertyType: 'Office', zipCode: '80202', Address: 'Colorado', expensesYear: '2021' },
            { reportDate: '11/1/2017', propertyName: 'property 23', propertyType: 'Retail', zipCode: '80202', Address: 'New york', expensesYear: '2020' },
            { reportDate: '20/11/2017', propertyName: 'property 13', propertyType: 'Retail', zipCode: '80216', Address: 'Utah', expensesYear: '2020' },
            { reportDate: '3/4/2018', propertyName: 'property 11', propertyType: 'Multi-family', zipCode: '80501', Address: 'La marsa', expensesYear: '2019' },
            { reportDate: '20/10/2017', propertyName: 'property 256', propertyType: 'Multi-family', zipCode: '80263', Address: 'Colorado', expensesYear: '2023' },
            { reportDate: '9/2/2088', propertyName: 'property 99', propertyType: 'Office', zipCode: '80648', Address: 'Colorado', expensesYear: '2020' },
        ];
    }
    ListReportsComponent.prototype.ngOnInit = function () {
    };
    ListReportsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-list-reports',
            template: __webpack_require__("../../../../../src/app/main/myreports/list-reports/list-reports.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/myreports/list-reports/list-reports.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ListReportsComponent);
    return ListReportsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/myreports/myreports.component.html":
/***/ (function(module, exports) {

module.exports = "<app-list-reports></app-list-reports>"

/***/ }),

/***/ "../../../../../src/app/main/myreports/myreports.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/myreports/myreports.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyreportsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MyreportsComponent = (function () {
    function MyreportsComponent() {
    }
    MyreportsComponent.prototype.ngOnInit = function () {
    };
    MyreportsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-myreports',
            template: __webpack_require__("../../../../../src/app/main/myreports/myreports.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/myreports/myreports.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MyreportsComponent);
    return MyreportsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/new-analysis/new-analysis.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header\" data-background-color=\"appraisal\">    \n                        <h4 class=\"title\">New Analysis</h4>                    \n                   </div>                \n                   <div class=\"card-content\">\n                    <form>\n                      <div class=\"row\">\n                          <div class=\"col-md-3\"></div>\n                          <div class=\"col-md-6\">   \n                                  <mat-panel-title>\n                                      Property Name\n                                  </mat-panel-title>\n                                  <mat-form-field style=\" width: 100%;\">\n                                      <input matInput  [(ngModel)]=\"propertyCharacteristic.propertyName\" name=\"propertyCharacteristic.propertyName\"  >\n                                 </mat-form-field>\n                            </div>  \n                          <div class=\"col-md-3\"> \n                       \n                          </div>\n                    </div>\n                    <br>\n                    <br>\n                    <br>\n\n             \n                          <div class=\"row\">\n                                <div class=\"col-md-3\"></div>\n                                <div class=\"col-md-6\">   \n                                    <mat-panel-title>   Property type\n\n                                      </mat-panel-title>\n                                       \n                              <mat-form-field  class=\"appraisal\" style=\" width: 100%;\">\n                                <mat-select [(ngModel)]=\"propertyCharacteristic.propertyType\" name=\"propertyCharacteristic.propertyType\" >\n                                  <mat-option *ngFor=\"let type of types\" [value]=\"type.value\">\n                                    {{ type.viewValue }}\n                                  </mat-option>\n                                </mat-select>\n                              </mat-form-field>\n\n                                  </div>  \n                                <div class=\"col-md-3\"> \n                             \n                                </div>\n                          </div>\n\n\n                          <br>\n                          <br>\n                          <br>\n                          <div class=\"row\">\n                       \n                           \n                              <div class=\"col-md-3\"></div>\n                                  \n                              \n                              <div class=\"col-md-6\">   \n                                    <mat-panel-title> \n                                      Year of Expense Data\n                                    </mat-panel-title>\n                                     \n                      <mat-form-field style=\" width: 100%;\">\n                    <mat-select [(ngModel)]=\"operatingExpenses.yearExpenseData\" name=\"operatingExpenses.yearExpenseData\" >\n                        <mat-option>None</mat-option>\n                        <mat-option value=\"2015\">2015</mat-option>\n                        <mat-option value=\"2016\">2016</mat-option>\n                        <mat-option value=\"2017\">2017</mat-option>\n                        <mat-option value=\"2018\">2018</mat-option>\n                        <mat-option value=\"2019\">2019</mat-option>\n                    </mat-select>\n                    </mat-form-field>\n                        \n                            </div>  \n                                    \n                              <div class=\"col-md-3\"></div>\n                              </div>\n    \n     \n                              <br>\n                              <br>\n                              <br>\n\n                              <button (click)=\"onNewAnalysis(operatingExpenses, propertyCharacteristic)\"  class=\"btn btn-appraisal\" >Start</button>\n     \n                    </form>\n                </div>\n            </div>\n          </div>\n    </div>\n  </div>\n  \n  <div class=\"card-content\">\n  \n  \n  \n  </div>"

/***/ }),

/***/ "../../../../../src/app/main/new-analysis/new-analysis.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/new-analysis/new-analysis.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewAnalysisComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_model_propertyCharacteristic__ = __webpack_require__("../../../../../src/app/model/propertyCharacteristic.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_model_OperatingExpense__ = __webpack_require__("../../../../../src/app/model/OperatingExpense.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_globals_service__ = __webpack_require__("../../../../../src/app/services/globals.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NewAnalysisComponent = (function () {
    function NewAnalysisComponent(globalsService, router) {
        this.globalsService = globalsService;
        this.router = router;
        this.propertyCharacteristic = new __WEBPACK_IMPORTED_MODULE_1_app_model_propertyCharacteristic__["a" /* PropertyCharacteristic */]();
        this.operatingExpenses = new __WEBPACK_IMPORTED_MODULE_2_app_model_OperatingExpense__["a" /* OperatingExpense */]();
        this.types = [
            { value: 'office', viewValue: 'Office' },
            { value: 'retail', viewValue: 'Retail' },
            { value: 'industrial', viewValue: 'Industrial' },
            { value: 'multiFamily', viewValue: 'Multi-Family' },
        ];
    }
    NewAnalysisComponent.prototype.onNewAnalysis = function (operatingExpenses, propertyCharacteristic) {
        this.operatingExpenses = this.globalsService.getOperatingExpense();
        this.propertyCharacteristic = this.globalsService.getPropertyCharacteristic();
        this.propertyCharacteristic.propertyName = propertyCharacteristic.propertyName;
        this.propertyCharacteristic.propertyType = propertyCharacteristic.propertyType;
        this.operatingExpenses.yearExpenseData = operatingExpenses.yearExpenseData;
        console.log(this.propertyCharacteristic);
        console.log(this.operatingExpenses);
        this.globalsService.setOperatingExpense(this.operatingExpenses);
        this.globalsService.setPropertyCharacteristic(this.propertyCharacteristic);
        this.router.navigate(['/home/new-property']);
    };
    NewAnalysisComponent.prototype.ngOnInit = function () {
    };
    NewAnalysisComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-new-analysis',
            template: __webpack_require__("../../../../../src/app/main/new-analysis/new-analysis.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/new-analysis/new-analysis.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_app_services_globals_service__["a" /* GlobalsService */], __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */]])
    ], NewAnalysisComponent);
    return NewAnalysisComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/new-data-entry/new-data-entry.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n\n  \n  <div class=\"container-fluid\">\n   \n              <div class=\"card\">                    \n                  <div class=\"card-header\" data-background-color=\"appraisal\">    \n                      <h4 class=\"title\">Operating Expenses Data Entry</h4>    \n                      <p class=\"category\">Data for Year {{ operatingExpense.yearExpenseData }}</p>                \n                 </div>                \n          <div class=\"card-content\">\n            <div class=\"row\">\n              <div class=\"col-md-1\"></div>\n        <div class=\"col-md-10\">\n      \n      <table class=\"table table-hover\">\n                <thead class=\"text-appraisal\">\n                        <tr>\n                                        <th></th>\n                                        <th></th>\n                                        <th>$/SF</th>\n                        </tr>\n                    \n                </thead>\n               \n   <tbody>\n                   \n                <tr style=\"background-color:#7b8d8e\">\n                <td>Effective Gross Income (EGI)</td> \n                <td><span><input  [(ngModel)]=\"operatingExpense.effectiveGrossIncome\" name=\"operatingExpense.effectiveGrossIncome\" ><i class=\"material-icons\" style=\"color:#eabb44; \">&#xE88E;</i></span></td>\n                <td class=\"xxx\">  {{ operatingExpense.effectiveGrossIncome / propertyCharacteristic.surface | currency}} </td>\n                </tr>\n    \n                <tr>      \n                <td>Administration</td> \n                <td><span><input [(ngModel)]=\"operatingExpense.administration\" name=\"operatingExpense.administration\" ><i class=\"material-icons\" style=\"color:#eabb44; \">&#xE88E;</i></span></td>\n                <td class=\"xxx\"> {{operatingExpense.administration / propertyCharacteristic.surface | currency}} </td>\n        </tr>\n                        \n              <tr>\n                      <td>Utilities</td>\n                      <td><span><input [(ngModel)]=\"operatingExpense.utilities\" name=\"operatingExpense.utilities\" ><i class=\"material-icons\" style=\"color:#eabb44; \">&#xE88E;</i></span></td>\n                      <td class=\"xxx\"> {{operatingExpense.utilities / propertyCharacteristic.surface | currency}}</td>\n              </tr>\n              <tr>\n                      <td>Payroll</td> \n                      <td> <span><input [(ngModel)]=\"operatingExpense.payroll\" name=\"operatingExpense.payroll\" > <i class=\"material-icons\" style=\"color:#eabb44; \">&#xE88E;</i></span></td>\n                      <td class=\"xxx\"> {{operatingExpense.payroll / propertyCharacteristic.surface | currency}} </td>\n              </tr>\n              <tr>\n                      <td>Repairs and Maintenance</td> \n                      <td><span><input [(ngModel)]=\"operatingExpense.repairsMaintenance\" name=\"operatingExpense.repairsMaintenance\" > <i class=\"material-icons\" style=\"color:#eabb44; \">&#xE88E;</i></span></td>\n                      <td class=\"xxx\"> {{operatingExpense.repairsMaintenance / propertyCharacteristic.surface | currency}}</td>\n              </tr>\n              <tr>\n                      <td>Cleaning & Janitorial</td> \n                      <td><span><input [(ngModel)]=\"operatingExpense.cleaningJanitorial\" name=\"operatingExpense.cleaningJanitorial\"  > <i class=\"material-icons\" style=\"color:#eabb44; \">&#xE88E;</i></span></td>\n                      <td class=\"xxx\"> {{operatingExpense.cleaningJanitorial / propertyCharacteristic.surface | currency}} </td>\n              </tr>\n              <tr>\n                      <td>CAM</td>\n                       <td><span><input [(ngModel)]=\"operatingExpense.cam\" name=\"operatingExpense.cam\" > <i class=\"material-icons\" style=\"color:#eabb44; \">&#xE88E;</i></span></td>\n                       <td class=\"xxx\"> {{operatingExpense.cam / propertyCharacteristic.surface | currency}}</td>\n              </tr>\n              <tr>\n                      <td>Real Estate Taxes</td>\n                       <td><span><input [(ngModel)]=\"operatingExpense.realEstateTaxes\" name=\"operatingExpense.realEstateTaxes\" > <i class=\"material-icons\" style=\"color:#eabb44; \">&#xE88E;</i></span></td>\n                       <td class=\"xxx\"> {{operatingExpense.realEstateTaxes / propertyCharacteristic.surface | currency}}</td>\n              </tr>\n              <tr>\n                      <td>Insurance</td>\n                       <td><span><input [(ngModel)]=\"operatingExpense.insurance\" name=\"operatingExpense.insurance\"  > <i class=\"material-icons\" style=\"color:#eabb44; \">&#xE88E;</i></span></td>\n                       <td class=\"xxx\"> {{operatingExpense.insurance / propertyCharacteristic.surface | currency }}</td>\n              </tr>\n              <tr>\n                      <td>Replacement Reserves</td>\n                       <td><span><input [(ngModel)]=\"operatingExpense.replacementReserves\" name=\"operatingExpense.replacementReserves\" > <i class=\"material-icons\" style=\"color:#eabb44; \">&#xE88E;</i></span></td>\n                       <td class=\"xxx\"> {{operatingExpense.replacementReserves / propertyCharacteristic.surface | currency }} </td>\n              </tr>\n                \n              <tr style=\"background-color:#7b8d8e\">\n                      <td>Total All Expenses</td> \n                      <td><span><input  [(ngModel)]=\"operatingExpense.totalAllExpenses\" name=\"operatingExpense.totalAllExpenses\" > <i class=\"material-icons\" style=\"color:#eabb44; \">&#xE88E;</i></span></td>\n                      <td class=\"xxx\"> {{operatingExpense.totalAllExpenses / propertyCharacteristic.surface | currency }} </td>\n              </tr>\n              <tr style=\"background-color:#7b8d8e\">\n                      <td>Net Operating Inc. (NOI)</td>\n                       <td><span><input [(ngModel)]=\"operatingExpense.netOperatingIncome\" name=\"operatingExpense.netOperatingIncome\" > <i class=\"material-icons\" style=\"color:#eabb44; \">&#xE88E;</i></span></td>\n                       <td class=\"xxx\"> {{operatingExpense.netOperatingIncome / propertyCharacteristic.surface | currency }} </td>\n              </tr>\n        </tbody>\n      </table>\n        \n      \n       </div> \n      <div class=\"col-md-3\">\n\n        \n      </div>\n\n      <button (click)=\"onOperatingExpensesDataEntry(operatingExpense)\"  class=\"btn btn-appraisal\" >next</button>\n        <button (click)=\"openDialog()\"  class=\"btn btn-appraisal\" >Add another year</button>\n      \n      <div class=\"col-md-1\"></div>\n      </div>\n           \n      </div>     \n    </div>\n  </div>\n</div>\n\n</div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/new-data-entry/new-data-entry.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".xxx {\n  width: 150px;\n  overflow: hidden;\n  display: inline-block;\n  white-space: nowrap; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/new-data-entry/new-data-entry.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewDataEntryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__popup_data_year_data_year_component__ = __webpack_require__("../../../../../src/app/main/popup/data-year/data-year.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_model_propertyCharacteristic__ = __webpack_require__("../../../../../src/app/model/propertyCharacteristic.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_model_OperatingExpense__ = __webpack_require__("../../../../../src/app/model/OperatingExpense.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_services_globals_service__ = __webpack_require__("../../../../../src/app/services/globals.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_app_services_data_services_operatingExpense_data_service__ = __webpack_require__("../../../../../src/app/services/data-services/operatingExpense-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_app_services_data_services_propertyCharacteristic_data_service__ = __webpack_require__("../../../../../src/app/services/data-services/propertyCharacteristic-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var NewDataEntryComponent = (function () {
    function NewDataEntryComponent(globalsService, router, dialog, operatingExpenseDataService, propertyCharacteristicDataService) {
        this.globalsService = globalsService;
        this.router = router;
        this.dialog = dialog;
        this.operatingExpenseDataService = operatingExpenseDataService;
        this.propertyCharacteristicDataService = propertyCharacteristicDataService;
        this.operatingExpense = new __WEBPACK_IMPORTED_MODULE_4_app_model_OperatingExpense__["a" /* OperatingExpense */]();
        this.propertyCharacteristic = new __WEBPACK_IMPORTED_MODULE_3_app_model_propertyCharacteristic__["a" /* PropertyCharacteristic */]();
    }
    NewDataEntryComponent.prototype.ngOnInit = function () {
        this.propertyCharacteristic = this.globalsService.getPropertyCharacteristic();
        this.operatingExpense = this.globalsService.getOperatingExpense();
    };
    NewDataEntryComponent.prototype.onOperatingExpensesDataEntry = function (operatingExpense) {
        this.propertyCharacteristic = this.globalsService.getPropertyCharacteristic();
        this.operatingExpense = this.globalsService.getOperatingExpense();
        this.operatingExpense.effectiveGrossIncome = this.operatingExpense.effectiveGrossIncome;
        this.operatingExpense.administration = this.operatingExpense.administration;
        this.operatingExpense.utilities = this.operatingExpense.utilities;
        this.operatingExpense.payroll = this.operatingExpense.payroll;
        this.operatingExpense.repairsMaintenance = this.operatingExpense.repairsMaintenance;
        this.operatingExpense.cleaningJanitorial = this.operatingExpense.cleaningJanitorial;
        this.operatingExpense.cam = this.operatingExpense.cam;
        this.operatingExpense.realEstateTaxes = this.operatingExpense.realEstateTaxes;
        this.operatingExpense.insurance = this.operatingExpense.insurance;
        this.operatingExpense.replacementReserves = this.operatingExpense.replacementReserves;
        this.operatingExpense.totalAllExpenses = this.operatingExpense.totalAllExpenses;
        this.operatingExpense.netOperatingIncome = this.operatingExpense.netOperatingIncome;
        this.total = (Number(this.operatingExpense.replacementReserves) + Number(this.operatingExpense.insurance) + Number(this.operatingExpense.realEstateTaxes) + Number(this.operatingExpense.cam) + Number(this.operatingExpense.cleaningJanitorial) + Number(this.operatingExpense.repairsMaintenance) + Number(this.operatingExpense.payroll) + Number(this.operatingExpense.utilities) + Number(this.operatingExpense.administration));
        if (this.total != this.operatingExpense.totalAllExpenses) {
            console.log(this.total);
            console.log(this.operatingExpense.totalAllExpenses);
            this.showNotification();
        }
        else {
            console.log(this.propertyCharacteristic);
            console.log(this.operatingExpense);
            this.globalsService.setOperatingExpense(this.operatingExpense);
            this.globalsService.setPropertyCharacteristic(this.propertyCharacteristic);
            this.onSave();
            this.router.navigate(['/home/analysis']);
        }
    };
    NewDataEntryComponent.prototype.onSave = function () {
        this.operatingExpenseDataService.creatOperatingExpense(this.operatingExpense)
            .subscribe(function (res) { return console.log("ajout operatingExpense effectuer dans la base de donné"); }, function (error) { return console.log("bug operatingExpense dans l ajout de la base de donné"); });
        this.propertyCharacteristicDataService.creatPropertyCharacteristic(this.propertyCharacteristic).subscribe(function (res) { return console.log("ajout propertyCharacteristic effectuer dans la base de donné"); }, function (error) { return console.log("bug propertyCharacteristic dans l ajout de la base de donné"); });
    };
    NewDataEntryComponent.prototype.openDialog = function () {
        var _this = this;
        if (this.total != this.operatingExpense.totalAllExpenses) {
            console.log(this.total);
            console.log(this.operatingExpense.totalAllExpenses);
            this.showNotification();
        }
        else {
            console.log(this.propertyCharacteristic);
            console.log(this.operatingExpense);
            this.globalsService.setOperatingExpense(this.operatingExpense);
            this.globalsService.setPropertyCharacteristic(this.propertyCharacteristic);
            this.onSave();
            this.operatingExpense = new __WEBPACK_IMPORTED_MODULE_4_app_model_OperatingExpense__["a" /* OperatingExpense */]();
            var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_2__popup_data_year_data_year_component__["a" /* DataYearComponent */], {
                width: '700px',
                data: {}
            });
            dialogRef.afterClosed().subscribe(function (res) {
                _this.operatingExpense.yearExpenseData = res;
            });
        }
    };
    NewDataEntryComponent.prototype.showNotification = function () {
        var type = ['', 'info', 'success', 'warning', 'danger'];
        $.notify({
            icon: "notifications",
            message: "the sum of your Operating Expenses is not equal to your totalAllExpenses"
        }, {
            type: type[4],
            timer: 4000,
            placement: {
                from: "bottom",
                align: "right"
            }
        });
    };
    NewDataEntryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-new-data-entry',
            template: __webpack_require__("../../../../../src/app/main/new-data-entry/new-data-entry.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/new-data-entry/new-data-entry.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_app_services_globals_service__["a" /* GlobalsService */], __WEBPACK_IMPORTED_MODULE_8__angular_router__["a" /* Router */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatDialog */], __WEBPACK_IMPORTED_MODULE_6_app_services_data_services_operatingExpense_data_service__["a" /* OperatingExpenseDataService */], __WEBPACK_IMPORTED_MODULE_7_app_services_data_services_propertyCharacteristic_data_service__["a" /* PropertyCharacteristicDataService */]])
    ], NewDataEntryComponent);
    return NewDataEntryComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/new-property/new-property.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\" >\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header\" data-background-color=\"appraisal\">    \n                        <h4 class=\"title\">Property Characteristics</h4>                    \n                   </div>                \n                   <div class=\"card-content\">\n                      <div class=\"row\">\n                        <form>\n                          <div class=\"col-md-1\"></div>\n                        <div class=\"col-md-10\">\n\n           <div class=\"row\">\n\n        \n             <div class=\"col-md-6\">\n    \n          <div class=\"row\">\n            <div class=\"col-md-12\">\n                <mat-form-field style=\" width: 100%;\">\n                    <mat-select placeholder=\"Property sub-type\" [(ngModel)]=\"propertyCharacteristic.propertySubType\" name=\"propertyCharacteristic.propertySubType\">\n                            <mat-option *ngFor=\"let type of subtypes\" [value]=\"type.value\">\n                              {{ type.viewValue }}\n                            </mat-option>\n                    </mat-select>\n                </mat-form-field>\n            </div>\n          </div>\n          <div class=\"row\">\n              <div class=\"col-md-12\">\n                  <mat-form-field style=\" width: 100%;\">\n                      <input matInput placeholder=\"Address\" [(ngModel)]=\"propertyCharacteristic.address\" name=\"propertyCharacteristic.address\">\n                </mat-form-field>\n              </div>\n            </div>\n\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <mat-form-field style=\" width: 100%;\">\n                    <mat-select placeholder=\"Zip Code\"  [(value)]=\"selected\" [(ngModel)]=\"propertyCharacteristic.zipCode\" name=\"propertyCharacteristic.zipCode\">\n                      <mat-option *ngFor=\"let code of codes\" [value]=\"code.value\" >\n                        {{ code.viewValue }}\n                      </mat-option>\n                    </mat-select>\n                </mat-form-field>\n            </div>\n        </div>\n\n             </div>\n             <div class=\"col-md-6\">\n            <div class=\"row\">\n              <div class=\"col-md-12\">\n                <agm-map [latitude]=\"lat\" [longitude]=\"lng\">\n                    <agm-marker [latitude]=\"lat\" [longitude]=\"lng\"></agm-marker>\n                  </agm-map>\n              </div>\n              <div class=\"col-md-12\">\n                  <div class=\"row\">\n                      <div class=\"col-md-12\">\n                          <h5 class=\"text-muted\"> City : {{propertyCharacteristic.city}}</h5>                         \n                      </div>                     \n                    </div>\n                    <div class=\"row\">\n                            <div class=\"col-md-12\">\n                                <h5 class=\"text-muted\"> MSA : {{propertyCharacteristic.msa}}</h5>                         \n                            </div>                     \n                          </div>\n                </div>\n            </div>\n          </div>\n          </div>\n           \n           <div class=\"row\">\n        \n                  <div class=\"row\">\n                      <div class=\"col-md-12\">\n                          <mat-form-field style=\" width: 100%;\">\n                              <input matInput placeholder=\"GBA\" [(ngModel)]=\"propertyCharacteristic.gba\" name=\"propertyCharacteristic.gba\">\n                        </mat-form-field>\n                      </div>\n                    </div>\n                    <div class=\"row\">\n                      <div class=\"col-md-12\">\n                          <mat-form-field style=\" width: 100%;\">\n                              <input matInput placeholder=\"NRA\" [(ngModel)]=\"propertyCharacteristic.nra\" name=\"propertyCharacteristic.nra\">\n                        </mat-form-field>\n                      </div>\n                    </div>\n                \n                <div class=\"row\">\n                    <div class=\"col-md-6\">\n                        <mat-form-field style=\" width: 100%;\">\n                            <input matInput placeholder=\"Number of Units\" [(ngModel)]=\"propertyCharacteristic.numberUnit\" name=\"propertyCharacteristic.numberUnit\">\n                      </mat-form-field>\n        \n                    </div>\n                    <div class=\"col-md-6\">\n                        <mat-form-field style=\" width: 100%;\">\n                            <input matInput placeholder=\"Average Unit Size\" [(ngModel)]=\"propertyCharacteristic.averageUnitSize\" name=\"propertyCharacteristic.averageUnitSize\">\n                      </mat-form-field>\n        \n                    </div>\n                </div>\n                       \n                <div class=\"row\">\n                    <div class=\"col-md-6\">\n                        <mat-form-field style=\" width: 100%;\">\n                            <input matInput placeholder=\"Rentable Area\" [(ngModel)]=\"propertyCharacteristic.rentableArea\" name=\"propertyCharacteristic.rentableArea\">\n                      </mat-form-field>\n        \n                    </div>\n                    <div class=\"col-md-6\">\n                        <mat-form-field style=\" width: 100%;\">\n                            <input matInput placeholder=\"Number of Tenants\" [(ngModel)]=\"propertyCharacteristic.numberTenants\" name=\"propertyCharacteristic.numberTenants\">\n                      </mat-form-field>\n        \n                    </div>\n                </div>             \n                <div class=\"row\">                  \n                    <div class=\"col-md-6\">\n                        <mat-form-field style=\" width: 100%;\">\n                            <input matInput placeholder=\"YOC\" [(ngModel)]=\"propertyCharacteristic.yoc\" name=\"propertyCharacteristic.yoc\">\n                      </mat-form-field>                  \n                  </div>\n                  <div class=\"col-md-6\">\n                      <mat-form-field style=\" width: 100%;\">\n                          <mat-select placeholder=\"Investment Class\" [(ngModel)]=\"propertyCharacteristic.investmentClass\" name=\"propertyCharacteristic.investmentClass\" >\n                            <mat-option *ngFor=\"let investment of investments\" [value]=\"investment.value\">\n                              {{ investment.viewValue }}\n                            </mat-option>\n                          </mat-select>\n                        </mat-form-field>\n                      </div>\n                </div>\n              <div class=\"row\">\n                  <div class=\"col-md-6\">\n                      <mat-form-field style=\" width: 100%;\">\n                          <input matInput placeholder=\"Year Built\" [(ngModel)]=\"propertyCharacteristic.yearBuilt\" name=\"propertyCharacteristic.yearBuilt\">\n                    </mat-form-field>\n                  </div>\n                  <div class=\"col-md-6\">\n                      <mat-form-field style=\" width: 100%;\">\n                          <input matInput placeholder=\"Year of Most Renovation\" [(ngModel)]=\"propertyCharacteristic.yearMostRenovation\" name=\"propertyCharacteristic.yearMostRenovation\">\n                    </mat-form-field>\n                  </div>\n                </div>\n                  \n            </div>\n             </div>\n             \n             <div class=\"col-md-1\"></div>\n\n             <button (click)=\"onPropertyCharacteristics(propertyCharacteristic)\"  class=\"btn btn-appraisal\" >next</button>\n     \n            </form>\n          </div>\n        \n        </div>\n    </div>\n    </div>\n            </div>\n          </div>\n    </div>\n"

/***/ }),

/***/ "../../../../../src/app/main/new-property/new-property.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "agm-map {\n  height: 300px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/new-property/new-property.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewPropertyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__model_OperatingExpense__ = __webpack_require__("../../../../../src/app/model/OperatingExpense.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_propertyCharacteristic__ = __webpack_require__("../../../../../src/app/model/propertyCharacteristic.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_globals_service__ = __webpack_require__("../../../../../src/app/services/globals.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NewPropertyComponent = (function () {
    function NewPropertyComponent(globalsService, router) {
        this.globalsService = globalsService;
        this.router = router;
        this.operatingExpenses = new __WEBPACK_IMPORTED_MODULE_1__model_OperatingExpense__["a" /* OperatingExpense */]();
        this.propertyCharacteristic = new __WEBPACK_IMPORTED_MODULE_2__model_propertyCharacteristic__["a" /* PropertyCharacteristic */]();
        this.title = 'My first AGM project';
        this.lat = 51.678418;
        this.lng = 7.809007;
        this.types = [
            { value: 'building', viewValue: 'Building' },
            { value: 'land', viewValue: 'Land' },
            { value: '...', viewValue: '...' }
        ];
        this.subtypes = [
            { value: 'SingleTenant', viewValue: 'Single Tenant' },
            { value: 'Multi-Tenant', viewValue: 'Multi-Tenant' },
            { value: '...', viewValue: '...' }
        ];
        this.citys = [
            { value: 'arvada', viewValue: 'Arvada' },
            { value: 'boulder', viewValue: 'Boulder' },
            { value: 'denver', viewValue: 'Denver' },
            { value: 'longmont', viewValue: 'Longmont' },
            { value: 'lakewood', viewValue: 'Lakewood' }
        ];
        this.codes = [
            { value: '80202', viewValue: '80202' },
            { value: '80080', viewValue: '80080' },
            { value: '80302', viewValue: '80302' },
            { value: '80501', viewValue: '80501' },
            { value: '80308', viewValue: '80308' }
        ];
        this.propertys = [
            { value: 'office', viewValue: 'Office' },
            { value: 'condominiumOfficeUnits', viewValue: 'Condominium office units' },
            { value: 'mixedUse', viewValue: 'Mixed use' },
            { value: 'retail', viewValue: 'Retail' },
        ];
        this.investments = [
            { value: 'A', viewValue: 'Class A (Top 10% of the market)' },
            { value: 'B', viewValue: 'Class B (Top 50% to 90% of the market)' },
            { value: 'C', viewValue: 'Class C (bottom 20% to 50% of the market)' },
            { value: 'D', viewValue: 'Class D (bottom 20% of the market)' }
        ];
    }
    NewPropertyComponent.prototype.ngOnInit = function () {
    };
    NewPropertyComponent.prototype.onPropertyCharacteristics = function (propertyCharacteristic) {
        this.propertyCharacteristic = this.globalsService.getPropertyCharacteristic();
        this.operatingExpenses = this.globalsService.getOperatingExpense();
        this.propertyCharacteristic.propertySubType = propertyCharacteristic.propertySubType;
        this.propertyCharacteristic.address = propertyCharacteristic.address;
        this.propertyCharacteristic.zipCode = propertyCharacteristic.zipCode;
        this.propertyCharacteristic.city = propertyCharacteristic.city;
        this.propertyCharacteristic.msa = propertyCharacteristic.msa;
        this.propertyCharacteristic.gba = propertyCharacteristic.gba;
        this.propertyCharacteristic.nra = propertyCharacteristic.nra;
        this.propertyCharacteristic.numberUnit = propertyCharacteristic.numberUnit;
        this.propertyCharacteristic.averageUnitSize = propertyCharacteristic.averageUnitSize;
        this.propertyCharacteristic.rentableArea = propertyCharacteristic.rentableArea;
        this.propertyCharacteristic.numberTenants = propertyCharacteristic.numberTenants;
        this.propertyCharacteristic.yoc = propertyCharacteristic.yoc;
        this.propertyCharacteristic.investmentClass = propertyCharacteristic.investmentClass;
        this.propertyCharacteristic.yearBuilt = propertyCharacteristic.yearBuilt;
        this.propertyCharacteristic.yearMostRenovation = propertyCharacteristic.yearMostRenovation;
        this.propertyCharacteristic.surface = (propertyCharacteristic.numberUnit * propertyCharacteristic.averageUnitSize);
        console.log(this.propertyCharacteristic);
        console.log(this.operatingExpenses);
        this.globalsService.setOperatingExpense(this.operatingExpenses);
        this.globalsService.setPropertyCharacteristic(this.propertyCharacteristic);
        this.router.navigate(['/home/new-data-entry']);
    };
    NewPropertyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-new-property',
            template: __webpack_require__("../../../../../src/app/main/new-property/new-property.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/new-property/new-property.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_app_services_globals_service__["a" /* GlobalsService */], __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */]])
    ], NewPropertyComponent);
    return NewPropertyComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/popup/data-year/data-year.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-toolbar style=\"width: 107.4%;\">Add another year for a more accurate report</mat-toolbar>\n<div class=\"contenu-blanc-avec-scroll\">\n   <div class=\"col-md-12 mx-auto\">\n        <form ngNativeValidate (ngSubmit)=\"onSubmit(user)\">\n           \n            <div class=\"card-content\">\n                <h4 class=\"card-title\"> Select a Year \n                  </h4>\n                                    \n      <mat-form-field style=\" width: 80%;\">\n    <mat-select [(value)]=\"selected\"  [(ngModel)]=\"operatingExpenses\" name=\"operatingExpenses\">\n        <mat-option>None</mat-option>        \n        <mat-option value=\"2012\">2012</mat-option>\n        <mat-option value=\"2013\">2013</mat-option>\n        <mat-option value=\"2014\">2014</mat-option>\n        <mat-option value=\"2015\">2015</mat-option>\n        <mat-option value=\"2016\">2016</mat-option>\n        <mat-option value=\"2017\">2017</mat-option>\n        <mat-option value=\"2018\">2018</mat-option>\n    </mat-select>\n    </mat-form-field>\n\n                <!-- <div class=\"form-group label-floating\">\n                    <input matInput type=\"texte\" class=\"form-control\" name=\"user.displayName\" [(ngModel)]=\"user.displayName\" required>\n                </div> -->\n\n          \n                <div class=\"text-center\">\n                    <button type=\"submit\" class=\"btn btn-appraisal btn-fill btn-wd\" (click)=\"onGo(operatingExpenses)\" >Go</button>\n                </div>\n                \n            </div>\n        </form>\n    </div>"

/***/ }),

/***/ "../../../../../src/app/main/popup/data-year/data-year.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-full-width {\n  width: 100%; }\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%; }\n\n.example-full-width {\n  width: 100%; }\n\n.mat-toolbar {\n  box-shadow: 0 10px 30px -12px rgba(0, 0, 0, 0.42), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);\n  -webkit-box-shadow: 0 10px 30px -12px rgba(0, 0, 0, 0.42), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);\n  /* box-shadow: 0 10px 30px -12px rgba(0, 0, 0, 0.42), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2); */\n  margin: -24px -22px 18px -24px;\n  padding: 15px;\n  background-color: #E45641;\n  color: #ffffff;\n  width: 100%; }\n\n.contenu-blanc-avec-scroll {\n  max-height: 600px;\n  overflow-y: auto; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/popup/data-year/data-year.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataYearComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var DataYearComponent = (function () {
    function DataYearComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    DataYearComponent.prototype.ngOnInit = function () {
    };
    DataYearComponent.prototype.onGo = function (selectedYear) {
        this.dialogRef.close(selectedYear);
    };
    DataYearComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-data-year',
            template: __webpack_require__("../../../../../src/app/main/popup/data-year/data-year.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/popup/data-year/data-year.component.scss")]
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["k" /* MatDialogRef */], Object])
    ], DataYearComponent);
    return DataYearComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/properties/list-propertie/list-propertie.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header\" data-background-color=\"appraisal\">\n                        <h4 class=\"title\">Properties list</h4>\n                        <p class=\"category cursorRight\"  [routerLink]=\"'/PropertyManager2'\">Add new Property<i class=\"material-icons\" >add_circle</i></p>\n                   </div>     \n                   <div class=\"row\">\n\n                        <div class=\"col-md-1\"></div>   \n                        \n                        <div class=\"col-md-10\">           \n                      <div class=\"card-content table-responsive\">\n                         \n                         \n                        <table class=\"table table-hover\">\n                          <thead class=\"text-appraisal\">\n                            <tr>\n                                <th>Property Name</th>\n                                <th>Property type</th>\n                                <th>Property sub-type</th>\n                                <th>Address</th>\n                                <th>City</th>\n                                <th>Zip Code</th>\n                        \n                            </tr>\n                        </thead>    \n                        <tbody>\n                            <tr>\n                              <td class=\"text\">Property 1</td>\n                              <td class=\"text\">building</td>\n                              <td class=\"text\">building</td>\n                              <td class=\"text\">7363 woodrow wilson dr</td>\n                              <td class=\"text\">boulder</td> \n                              <td class=\"text\">80202</td>\n                            </tr> \n                            <tr>\n                                <td class=\"text\">Property 2</td>\n                                <td class=\"text\">building</td>\n                                <td class=\"text\">building</td>\n                                <td class=\"text\">12 McClane Street</td>\n                                <td class=\"text\">lakewood</td>\n                                <td class=\"text\">80080</td>\n                              </tr> \n\n                            <tr>\n                                <td class=\"text\">Property 3</td>\n                                <td class=\"text\">Land</td>\n                                <td class=\"text\">building</td>\n                                <td class=\"text\">6845 woodrow dr</td>\n                                <td class=\"text\">denver</td>\n                                <td class=\"text\">80302</td>\n                              </tr> \n                       </tbody>\n                       </table>\n                    </div>\n                </div>\n                <div class=\"col-md-1\"></div>\n                    </div>\n                </div>\n            </div>\n          </div>\n    </div>\n  </div>"

/***/ }),

/***/ "../../../../../src/app/main/properties/list-propertie/list-propertie.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "tr.small-item-block td {\n  margin-top: 0 !important;\n  padding-top: 0 !important; }\n\n.cursorRight {\n  margin-left: 80%;\n  cursor: pointer; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/properties/list-propertie/list-propertie.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPropertieComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ListPropertieComponent = (function () {
    function ListPropertieComponent() {
    }
    ListPropertieComponent.prototype.ngOnInit = function () {
    };
    ListPropertieComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-list-propertie',
            template: __webpack_require__("../../../../../src/app/main/properties/list-propertie/list-propertie.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/properties/list-propertie/list-propertie.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ListPropertieComponent);
    return ListPropertieComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/properties/popup-propertie/popup-propertie.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  popup-propertie works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/main/properties/popup-propertie/popup-propertie.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/properties/popup-propertie/popup-propertie.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupPropertieComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PopupPropertieComponent = (function () {
    function PopupPropertieComponent() {
    }
    PopupPropertieComponent.prototype.ngOnInit = function () {
    };
    PopupPropertieComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-popup-propertie',
            template: __webpack_require__("../../../../../src/app/main/properties/popup-propertie/popup-propertie.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/properties/popup-propertie/popup-propertie.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PopupPropertieComponent);
    return PopupPropertieComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/properties/properties.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"card-header\" data-background-color=\"appraisal\">\n                      <h4 class=\"title\">Operating costs data</h4>\n                      <button mat-raised-button color=\"red\">Warn</button>\n                 </div>     \n                 <div class=\"row\">\n\n                      <div class=\"col-md-1\"></div>   \n                      \n                      <div class=\"col-md-10\">           \n                    <div class=\"card-content table-responsive\">\n                       \n                       \n                      <table class=\"table table-hover\">\n                        <thead class=\"text-appraisal\">\n                          <tr>\n                              <th></th>\n                              <th></th>\n                              <th colspan=\"3\"><span style=\"padding-left:23% ;\" >Range</span> </th>\n                          </tr>\n                          <tr>\n                              <th></th>\n                              <th>MED</th>\n                              <th>LOW</th>\n                              <th>Hight</th>                  \n                          </tr>\n                      </thead>\n                      <tbody>\n                          <tr *ngFor=\"let appraisal of appraisals\"  >\n                            <td >{{appraisal.value}}</td>\n                            <td class=\"text\">{{appraisal.med}} $</td>\n                    \n                            <td class=\"text\">{{appraisal.low}} $</td>\n                            <td class=\"text\">{{appraisal.high}} $</td>\n                          </tr> \n                          <tr >\n                              <td class=\"text-danger\" > <h4>TOTAL : </h4></td>\n                              <td class=\"text-danger\">261, 388 $</td>\n                              <td class=\"text-danger\">104, 444 $</td>\n                              <td class=\"text-danger\">506, 666 $</td>\n                            </tr> \n                     </tbody>\n                     </table>\n                  </div>\n              </div>\n              <div class=\"col-md-1\"></div>\n                  </div>\n              </div>\n          </div>\n        </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/properties/properties.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/properties/properties.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PropertiesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PropertiesComponent = (function () {
    function PropertiesComponent() {
    }
    PropertiesComponent.prototype.ngOnInit = function () {
    };
    PropertiesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-properties',
            template: __webpack_require__("../../../../../src/app/main/properties/properties.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/properties/properties.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PropertiesComponent);
    return PropertiesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/model/OperatingExpense.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OperatingExpense; });
var OperatingExpense = (function () {
    function OperatingExpense() {
    }
    return OperatingExpense;
}());



/***/ }),

/***/ "../../../../../src/app/model/User.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
/* unused harmony export Type */
var User = (function () {
    function User() {
    }
    return User;
}());

var Type = (function () {
    function Type() {
    }
    return Type;
}());



/***/ }),

/***/ "../../../../../src/app/model/propertyCharacteristic.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PropertyCharacteristic; });
var PropertyCharacteristic = (function () {
    function PropertyCharacteristic() {
    }
    return PropertyCharacteristic;
}());



/***/ }),

/***/ "../../../../../src/app/services/auth/af.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AfService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__("../../../../angularfire2/auth/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_firestore__ = __webpack_require__("../../../../angularfire2/firestore/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase_app__ = __webpack_require__("../../../../firebase/app/dist/index.cjs.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__("../../../../rxjs-compat/_esm5/Observable.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AfService = (function () {
    function AfService(afAuth, afS) {
        var _this = this;
        this.afAuth = afAuth;
        this.afS = afS;
        this.user$ = afAuth.authState.switchMap(function (user) {
            if (user) {
                return _this.afS.doc('users/$(user.uid)').valueChanges();
            }
            else {
                return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["a" /* Observable */].of(null);
            }
        });
    }
    AfService.prototype.login = function (email, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signInWithEmailAndPassword(email, password)
                .then(function (userData) { return resolve(userData); }, function (err) { return reject(err); });
        });
    };
    AfService.prototype.loginWithGoogle = function () {
        var _this = this;
        var provider = new __WEBPACK_IMPORTED_MODULE_3_firebase_app__["auth"].GoogleAuthProvider();
        this.afAuth.auth.signInWithPopup(provider).then((function (credential) {
            _this.updateUser(credential.user);
        }));
    };
    AfService.prototype.updateUser = function (user) {
        var userRef = this.afS.doc('users/$(user.uid)');
        var data = {
            uid: user.uid,
            firstName: user.firstName,
            lastName: user.lastName,
            type: {
                dev: true,
                admin: false
            },
            city: user.city,
            email: user.email,
        };
        return userRef.set(data, { merge: true });
    };
    AfService.prototype.getAuth = function () {
        return this.afAuth.authState.map(function (auth) { return auth; });
    };
    AfService.prototype.register = function (email, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.createUserWithEmailAndPassword(email, password)
                .then(function (userData) { return resolve(userData); }, function (err) { return reject(err); });
        });
    };
    AfService.prototype.logout = function () {
        this.afAuth.auth.signOut();
    };
    AfService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_firestore__["a" /* AngularFirestore */]])
    ], AfService);
    return AfService;
}());



/***/ }),

/***/ "../../../../../src/app/services/auth/navbar.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavbarService = (function () {
    function NavbarService() {
        this.visible = false;
    }
    NavbarService.prototype.hide = function () { this.visible = false; };
    NavbarService.prototype.show = function () { this.visible = true; };
    NavbarService.prototype.toggle = function () { this.visible = !this.visible; };
    NavbarService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], NavbarService);
    return NavbarService;
}());



/***/ }),

/***/ "../../../../../src/app/services/data-services/operatingExpense-data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OperatingExpenseDataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs-compat/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OperatingExpenseDataService = (function () {
    function OperatingExpenseDataService(http) {
        this.http = http;
    }
    OperatingExpenseDataService.prototype.creatOperatingExpense = function (operatingExpense) {
        return this.http.post('https://webapplication-201615.firebaseio.com/operatingExpense.json', operatingExpense);
    };
    OperatingExpenseDataService.prototype.getOperatingExpense = function () {
        return this.http.get("https://webapplication-201615.firebaseio.com/operatingExpense.json")
            .map(function (response) { return response.json(); });
    };
    OperatingExpenseDataService.prototype.getOneOperatingExpense = function (id) {
        return this.http.get("https://webapplication-201615.firebaseio.com/operatingExpense/" + id + ".json")
            .map(function (response) { return response.json(); });
    };
    OperatingExpenseDataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], OperatingExpenseDataService);
    return OperatingExpenseDataService;
}());



/***/ }),

/***/ "../../../../../src/app/services/data-services/propertyCharacteristic-data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PropertyCharacteristicDataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs-compat/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PropertyCharacteristicDataService = (function () {
    function PropertyCharacteristicDataService(http) {
        this.http = http;
    }
    PropertyCharacteristicDataService.prototype.creatPropertyCharacteristic = function (propertyCharacteristic) {
        return this.http.post('https://webapplication-201615.firebaseio.com/propertyCharacteristic.json', propertyCharacteristic);
    };
    PropertyCharacteristicDataService.prototype.getPropertyCharacteristic = function () {
        return this.http.get("https://webapplication-201615.firebaseio.com/propertyCharacteristic.json")
            .map(function (response) { return response.json(); });
    };
    PropertyCharacteristicDataService.prototype.getOnePropertyCharacteristic = function (id) {
        return this.http.get("https://webapplication-201615.firebaseio.com/propertyCharacteristic/" + id + ".json")
            .map(function (response) { return response.json(); });
    };
    PropertyCharacteristicDataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], PropertyCharacteristicDataService);
    return PropertyCharacteristicDataService;
}());



/***/ }),

/***/ "../../../../../src/app/services/data-services/user-data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserDataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs-compat/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserDataService = (function () {
    function UserDataService(http) {
        this.http = http;
    }
    UserDataService.prototype.creatUser = function (user) {
        return this.http.post('https://webapplication-201615.firebaseio.com/user.json', user);
    };
    UserDataService.prototype.getUser = function () {
        return this.http.get("https://webapplication-201615.firebaseio.com/user.json")
            .map(function (response) { return response.json(); });
    };
    UserDataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], UserDataService);
    return UserDataService;
}());



/***/ }),

/***/ "../../../../../src/app/services/globals.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_model_propertyCharacteristic__ = __webpack_require__("../../../../../src/app/model/propertyCharacteristic.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_model_OperatingExpense__ = __webpack_require__("../../../../../src/app/model/OperatingExpense.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GlobalsService = (function () {
    function GlobalsService() {
        this.oeratingExpenseVariable = new __WEBPACK_IMPORTED_MODULE_2_app_model_OperatingExpense__["a" /* OperatingExpense */]();
        this.propertyCharacteristicVariable = new __WEBPACK_IMPORTED_MODULE_1_app_model_propertyCharacteristic__["a" /* PropertyCharacteristic */]();
    }
    GlobalsService.prototype.getOperatingExpense = function () {
        return this.oeratingExpenseVariable;
    };
    GlobalsService.prototype.getPropertyCharacteristic = function () {
        return this.propertyCharacteristicVariable;
    };
    GlobalsService.prototype.setOperatingExpense = function (oeratingExpenseVariable) {
        return this.oeratingExpenseVariable = oeratingExpenseVariable;
    };
    GlobalsService.prototype.setPropertyCharacteristic = function (propertyCharacteristicVariable) {
        return this.propertyCharacteristicVariable = propertyCharacteristicVariable;
    };
    GlobalsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], GlobalsService);
    return GlobalsService;
}());



/***/ }),

/***/ "../../../../../src/app/services/guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__("../../../../angularfire2/auth/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthGuard = (function () {
    function AuthGuard(router, afAuth) {
        this.router = router;
        this.afAuth = afAuth;
    }
    AuthGuard.prototype.canActivate = function () {
        var _this = this;
        return this.afAuth.authState.map(function (auth) {
            if (!auth) {
                _this.router.navigate(['/']);
                return false;
            }
            else {
                return true;
            }
        });
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "../../../../../src/app/services/pipes/keys.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KeysPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var KeysPipe = (function () {
    function KeysPipe() {
    }
    KeysPipe.prototype.transform = function (value, args) {
        var keys = [];
        for (var key in value) {
            keys.push({ key: key, value: value[key] });
        }
        return keys;
    };
    KeysPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Pipe */])({ name: 'keys' })
    ], KeysPipe);
    return KeysPipe;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyAOAB3zEWzZok5s45SvIxBf9u1SIY1rf8A',
        authDomain: 'webapplication-201615.firebaseapp.com',
        databaseURL: 'https://webapplication-201615.firebaseio.com',
        projectId: 'webapplication-201615',
        storageBucket: 'webapplication-201615.appspot.com',
        messagingSenderId: '887614638047'
    }
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hammerjs__ = __webpack_require__("../../../../hammerjs/hammer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_hammerjs__);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_17" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map