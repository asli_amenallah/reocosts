package core;

import com.google.gson.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class MailHandlerServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		// 1. get received JSON data from request
		BufferedReader br = new BufferedReader(new InputStreamReader(
				req.getInputStream()));
		String json = "";
		if (br != null) {
			json = br.readLine();
		}

		Gson gson = new Gson();
		Contact contact = gson.fromJson(json, Contact.class);

		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		String name = contact.getName();
		String description = contact.getDescription();
		String email = contact.getEmail();

		String msgBody = "Name:" + name  + "\n" + "Description:"+ description +
				"\n" +"EMAIL:" + email  ;

		try {
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("noreply@wizy.io",
					"User"));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
					"contact@wizy.io", "Wizy-Admin"));
			msg.setSubject("Email from "+ name);
			msg.setText(msgBody);
			Transport.send(msg);

		} catch (AddressException e) {
			resp.setContentType("text");
			resp.getWriter().println(
					"Oops! There was a problem with your submission. Please complete the form and try again.");
		} catch (MessagingException e) {
			resp.setContentType("text");
			resp.getWriter().println(
					"Oops! There was a problem with your submission. Please complete the form and try again.");
		}
		resp.setContentType("text");
		resp.getWriter().println(" Thank you for reaching out to wizy.io. We'll get back in touch with you very soon to address your questions and concerns.");

	}
}