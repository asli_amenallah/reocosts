package core;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.MessagingException;
import com.google.gson.Gson;

public class MailHandlerServletSubscribe extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		// 1. get received JSON data from request
		BufferedReader br = new BufferedReader(new InputStreamReader(
				req.getInputStream()));
		String json = "";
		if (br != null) {
			json = br.readLine();
		}

		Gson gson = new Gson();
		Contact contact = gson.fromJson(json, Contact.class);

		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		String name = contact.getName();
		String description = contact.getDescription();
		String email = contact.getEmail();

		// String msgBody = "Name: " + name  + "\n" + 
		// 		"\n" +"EMAIL: " + email  ;

		String msgBody = "Name: " + name  + "\n" + "Description: "+ description +
				"\n" +"EMAIL: " + email  ;

		try {
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("noreply@wizy.io",
					"User"));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
					"contact@wizy.io", "Wizy-Admin"));
			msg.setSubject("Email from "+ name);
			msg.setText(msgBody);
			Transport.send(msg);

		} catch (AddressException e) {
			resp.setContentType("text");
			resp.getWriter().println(
					"Oops! There was a problem with your subscription. Please complete the form and try again.");
		} catch (MessagingException e) {
			resp.setContentType("text");
			resp.getWriter().println(
					"Oops! There was a problem with your subscription. Please complete the form and try again.");
		}
		resp.setContentType("text");
		resp.getWriter().println(
				"Thanks for your subscription.");

	}
}