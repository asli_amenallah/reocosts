import { Component, OnInit } from '@angular/core';
import { isNull } from 'util';
import { NavbarService } from 'app/services/auth/navbar.service';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    submenu?: any [];
}
export const ROUTES: RouteInfo[] = [
   
  

    { path: 'javascript:void(0)', title: 'Project',  icon: 'work', class: '', submenu: [
      {path: 'new-analysis', title: 'New Analysis',  mini:'', icon: 'dashboard', class: 'notactive'},
      {path: 'list', title: 'List',  mini:'', icon: 'dashboard', class: 'notactive'},
    ]},


    { path: 'my-reports', title: 'My Reports',  icon: 'dashboard', class: ''},
    { path: 'user-admin', title: 'User Admin',  icon: 'dashboard', class: ''},
    { path: 'config', title: 'Configuration',  icon: 'dashboard', class: ''},
];
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(public nav: NavbarService) { }
  showNav = true;
  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);

  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
