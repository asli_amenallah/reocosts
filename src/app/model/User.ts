export class User {
    uid?: string;
    firstName?: string;
    lastName?: string;
    type?:Type;
    city?:string;
    email:string;    
    job?:string;
}
export class Type {
    dev ?: boolean;
    admin ?: boolean; 
}