export class OperatingExpense {
    effectiveGrossIncome: number;
    administration: number;
    utilities:number;
    payroll:number;
    repairsMaintenance:number;
    cleaningJanitorial:number;
    cam:number;
    realEstateTaxes:number;
    insurance:number;
    replacementReserves:number;
    totalAllExpenses :number;
    netOperatingIncome:number;
    yearExpenseData:number;
}