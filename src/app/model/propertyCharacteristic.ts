export class PropertyCharacteristic {
    propertyName: string;
    propertyType: string;
    propertySubType:string;
    address:string;
    zipCode:string;
    city:string;
    msa:string;
    gba:string;
    nra:string;
    numberUnit :number;
    averageUnitSize:number;
    rentableArea:number;
    numberTenants:number;
    yoc:string;
    investmentClass:string;	
    yearBuilt:string;
    surface: number;
    yearMostRenovation:string;
}