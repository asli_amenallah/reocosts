import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'D:/Documents/reocosts/node_modules/angularfire2/database';
import { Observable } from 'rxjs/observable';


@Component({
  selector: 'app-list-mydata',
  templateUrl: './list-mydata.component.html',
  styleUrls: ['./list-mydata.component.scss']
})
export class ListMydataComponent implements OnInit {
dataObservable : Observable<any[]>;
  constructor(private db : AngularFireDatabase) { }

  ngOnInit() {
  
this.dataObservable = this.takeData('/operatingExpense')
  }

  takeData(myPath) : Observable<any[]> {
  return  this.db.list(myPath).valueChanges();
  }
}
