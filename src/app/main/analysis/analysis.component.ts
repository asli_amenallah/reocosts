import { Component, OnInit } from '@angular/core';
import { OperatingExpenseDataService } from 'app/services/data-services/operatingExpense-data.service';
import { OperatingExpense } from 'app/model/OperatingExpense';
import { GlobalsService } from 'app/services/globals.service';
import { PropertyCharacteristic } from 'app/model/propertyCharacteristic';
import { Router } from '@angular/router';

@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.scss']
})
export class AnalysisComponent implements OnInit {
 
  operatingExpense = new OperatingExpense () ;
  propertyCharacteristic = new PropertyCharacteristic () ;
  
  constructor(private globalsService : GlobalsService, private operatingExpenseDataService : OperatingExpenseDataService,private router : Router) {
   
  }
  ngOnInit() {
    
    this.propertyCharacteristic  = this.globalsService.getPropertyCharacteristic();
    this.operatingExpense  = this.globalsService.getOperatingExpense();
  }

  ConvertToInt(currentPage){
    return parseInt(currentPage);
  }
total: number ;

}
