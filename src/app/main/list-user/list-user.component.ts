import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
 users = [
   {firstName : 'david', lastName:'du pont', im :'building owner', email :'david@gmail.com' },

   {firstName : 'angel', lastName:'delavega', im :'tax assessor', email :'angel@yahoo.com' },

   {firstName : 'angelina', lastName:'jolie', im :'tenant', email :'buffy@gmail.com' },
   {firstName : 'momo', lastName:'smith', im :'appraiser', email :'buffy@gmail.com' },
    
 ]
 
}
