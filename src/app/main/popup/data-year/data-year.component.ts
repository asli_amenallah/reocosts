import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-data-year',
  templateUrl: './data-year.component.html',
  styleUrls: ['./data-year.component.scss']
})
export class DataYearComponent implements OnInit {

  operatingExpenses: string;
  ngOnInit() {
    }
  constructor(public dialogRef: MatDialogRef<DataYearComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
    }
  onGo(selectedYear){
    this.dialogRef.close(selectedYear);
  }
}
