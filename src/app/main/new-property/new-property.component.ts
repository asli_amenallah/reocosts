import { Component, OnInit } from '@angular/core';
import { OperatingExpense } from '../../model/OperatingExpense';
import { PropertyCharacteristic } from '../../model/propertyCharacteristic';
import { MatStepper } from '@angular/material/stepper';
import { GlobalsService } from 'app/services/globals.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-new-property',
  templateUrl: './new-property.component.html',
  styleUrls: ['./new-property.component.scss']
})
export class NewPropertyComponent implements OnInit {



   operatingExpenses = new OperatingExpense () ;
   propertyCharacteristic = new PropertyCharacteristic () ;

  title: string = 'My first AGM project';
  lat: number = 51.678418;
  lng: number = 7.809007;
  
  constructor(private globalsService : GlobalsService, private router : Router) { }

  ngOnInit() {

  }
  onPropertyCharacteristics(propertyCharacteristic : PropertyCharacteristic) {
    this.propertyCharacteristic  = this.globalsService.getPropertyCharacteristic();
    this.operatingExpenses  = this.globalsService.getOperatingExpense();

    this.propertyCharacteristic.propertySubType = propertyCharacteristic.propertySubType;
    this.propertyCharacteristic.address = propertyCharacteristic.address;
    this.propertyCharacteristic.zipCode = propertyCharacteristic.zipCode;
    this.propertyCharacteristic.city = propertyCharacteristic.city;
    this.propertyCharacteristic.msa = propertyCharacteristic.msa;
    this.propertyCharacteristic.gba = propertyCharacteristic.gba;
    this.propertyCharacteristic.nra = propertyCharacteristic.nra;
    this.propertyCharacteristic.numberUnit = propertyCharacteristic.numberUnit;
    this.propertyCharacteristic.averageUnitSize = propertyCharacteristic.averageUnitSize;
    this.propertyCharacteristic.rentableArea = propertyCharacteristic.rentableArea;
    this.propertyCharacteristic.numberTenants = propertyCharacteristic.numberTenants;
    this.propertyCharacteristic.yoc = propertyCharacteristic.yoc;
    this.propertyCharacteristic.investmentClass = propertyCharacteristic.investmentClass;
    this.propertyCharacteristic.yearBuilt = propertyCharacteristic.yearBuilt;
    this.propertyCharacteristic.yearMostRenovation = propertyCharacteristic.yearMostRenovation;
    this.propertyCharacteristic.surface = (propertyCharacteristic.numberUnit * propertyCharacteristic.averageUnitSize);
    
    console.log(this.propertyCharacteristic);
    console.log(this.operatingExpenses);
    
      this.globalsService.setOperatingExpense(this.operatingExpenses);
      this.globalsService.setPropertyCharacteristic(this.propertyCharacteristic);
      this.router.navigate(['/home/new-data-entry']);
    }

  types = [
    {value: 'building', viewValue: 'Building'},
    {value: 'land', viewValue: 'Land'},
    {value: '...', viewValue: '...'}
  ];
  subtypes = [
  
    {value: 'SingleTenant', viewValue: 'Single Tenant'},
    {value: 'Multi-Tenant', viewValue: 'Multi-Tenant'},
    {value: '...', viewValue: '...'}
  ];
  citys = [
    {value: 'arvada', viewValue: 'Arvada'},
    {value: 'boulder', viewValue: 'Boulder'},
    {value: 'denver', viewValue: 'Denver'},
    {value: 'longmont', viewValue: 'Longmont'},
    {value: 'lakewood', viewValue: 'Lakewood'}
  ];
  codes = [
    {value: '80202', viewValue: '80202'},
    {value: '80080', viewValue: '80080'},
    {value: '80302', viewValue: '80302'},
    {value: '80501', viewValue: '80501'},
    {value: '80308', viewValue: '80308'}
  ];
  
  propertys = [
    {value: 'office', viewValue: 'Office'},
    {value: 'condominiumOfficeUnits', viewValue: 'Condominium office units'},
    {value: 'mixedUse', viewValue: 'Mixed use'},
    {value: 'retail', viewValue: 'Retail'},
  ];
  investments = [
    {value: 'A', viewValue: 'Class A (Top 10% of the market)'},
    {value: 'B', viewValue: 'Class B (Top 50% to 90% of the market)'},
    {value: 'C', viewValue: 'Class C (bottom 20% to 50% of the market)'},
    {value:'D', viewValue:'Class D (bottom 20% of the market)'}
  ];
}
