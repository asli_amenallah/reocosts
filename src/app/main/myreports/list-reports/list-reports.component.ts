import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-reports',
  templateUrl: './list-reports.component.html',
  styleUrls: ['./list-reports.component.scss']
})
export class ListReportsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  reports = [
    {reportDate:'1/10/2018', propertyName:'property 1', propertyType:'Office',zipCode:'80202',Address:'Colorado' ,expensesYear:'2021'},
    {reportDate:'11/1/2017', propertyName:'property 23', propertyType:'Retail',zipCode:'80202',Address:'New york' ,expensesYear:'2020'},
    {reportDate:'20/11/2017', propertyName:'property 13', propertyType:'Retail',zipCode:'80216',Address:'Utah' ,expensesYear:'2020'},
    {reportDate:'3/4/2018', propertyName:'property 11', propertyType:'Multi-family',zipCode:'80501',Address:'La marsa' ,expensesYear:'2019'},
    {reportDate:'20/10/2017', propertyName:'property 256', propertyType:'Multi-family',zipCode:'80263',Address:'Colorado' ,expensesYear:'2023'},
    {reportDate:'9/2/2088', propertyName:'property 99', propertyType:'Office',zipCode:'80648',Address:'Colorado' ,expensesYear:'2020'},
  ]
}
