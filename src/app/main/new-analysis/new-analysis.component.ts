import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatStepper} from '@angular/material';

import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import { PropertyCharacteristic } from 'app/model/propertyCharacteristic';
import { OperatingExpense } from 'app/model/OperatingExpense';
import { GlobalsService } from 'app/services/globals.service';
import { Router } from '@angular/router';
import { NavbarService } from '../../services/auth/navbar.service';

@Component({
  selector: 'app-new-analysis',
  templateUrl: './new-analysis.component.html',
  styleUrls: ['./new-analysis.component.scss']
})


export class NewAnalysisComponent implements OnInit {

  private  propertyCharacteristic = new PropertyCharacteristic();
  private operatingExpenses  = new OperatingExpense();

  types = [
    {value: 'office', viewValue: 'Office'},
    {value: 'retail', viewValue: 'Retail'},
    {value: 'industrial', viewValue: 'Industrial'},
    {value: 'multiFamily', viewValue: 'Multi-Family'},
  ];


  constructor(private globalsService : GlobalsService,private router : Router , private nav : NavbarService) { 

  }
  onNewAnalysis(operatingExpenses :OperatingExpense, propertyCharacteristic : PropertyCharacteristic) {

    this.operatingExpenses  = this.globalsService.getOperatingExpense();
    this.propertyCharacteristic  = this.globalsService.getPropertyCharacteristic();
    
    this.propertyCharacteristic.propertyName = propertyCharacteristic.propertyName;
    this.propertyCharacteristic.propertyType = propertyCharacteristic.propertyType;
    this.operatingExpenses.yearExpenseData = operatingExpenses.yearExpenseData;
    console.log(this.propertyCharacteristic);
    console.log(this.operatingExpenses);
    
      this.globalsService.setOperatingExpense(this.operatingExpenses);
      this.globalsService.setPropertyCharacteristic(this.propertyCharacteristic);
      this.router.navigate(['/home/new-property']);
    }

  ngOnInit() {
    this.nav.show();
  }


}
