import { Component, OnInit } from '@angular/core';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DataYearComponent } from '../popup/data-year/data-year.component';
import { PropertyCharacteristic } from 'app/model/propertyCharacteristic';
import { OperatingExpense } from 'app/model/OperatingExpense';

import { GlobalsService } from 'app/services/globals.service';
import { OperatingExpenseDataService } from 'app/services/data-services/operatingExpense-data.service';
import { PropertyCharacteristicDataService } from 'app/services/data-services/propertyCharacteristic-data.service';
import { Router } from '@angular/router';

declare var $: any;
@Component({
  selector: 'app-new-data-entry',
  templateUrl: './new-data-entry.component.html',
  styleUrls: ['./new-data-entry.component.scss']
})
export class NewDataEntryComponent implements OnInit {

  operatingExpense = new OperatingExpense () ;
  propertyCharacteristic = new PropertyCharacteristic () ;
  
  
  total : number;

  constructor(private globalsService : GlobalsService,private router : Router, public dialog: MatDialog, private operatingExpenseDataService : OperatingExpenseDataService, private propertyCharacteristicDataService : PropertyCharacteristicDataService ) { 


  }

  ngOnInit() {

    this.propertyCharacteristic  = this.globalsService.getPropertyCharacteristic();
    this.operatingExpense  = this.globalsService.getOperatingExpense();
  }
  
  onOperatingExpensesDataEntry(operatingExpense : OperatingExpense) {
    this.propertyCharacteristic  = this.globalsService.getPropertyCharacteristic();
    this.operatingExpense  = this.globalsService.getOperatingExpense();

    this.operatingExpense.effectiveGrossIncome = this.operatingExpense.effectiveGrossIncome;
    this.operatingExpense.administration = this.operatingExpense.administration;
    this.operatingExpense.utilities = this.operatingExpense.utilities;
    this.operatingExpense.payroll = this.operatingExpense.payroll;
    this.operatingExpense.repairsMaintenance = this.operatingExpense.repairsMaintenance;
    this.operatingExpense.cleaningJanitorial = this.operatingExpense.cleaningJanitorial;
    this.operatingExpense.cam = this.operatingExpense.cam;
    this.operatingExpense.realEstateTaxes = this.operatingExpense.realEstateTaxes;
    this.operatingExpense.insurance = this.operatingExpense.insurance;
    this.operatingExpense.replacementReserves = this.operatingExpense.replacementReserves;  
    this.operatingExpense.totalAllExpenses = this.operatingExpense.totalAllExpenses;
    this.operatingExpense.netOperatingIncome = this.operatingExpense.netOperatingIncome;
    
    this.total = (Number(this.operatingExpense.replacementReserves) + Number(this.operatingExpense.insurance) + Number(this.operatingExpense.realEstateTaxes) + Number(this.operatingExpense.cam) +  Number(this.operatingExpense.cleaningJanitorial) +  Number(this.operatingExpense.repairsMaintenance) +  Number(this.operatingExpense.payroll) + Number(this.operatingExpense.utilities) + Number(this.operatingExpense.administration) )
   
    if (this.total != this.operatingExpense.totalAllExpenses) {
      console.log(this.total);
      console.log(this.operatingExpense.totalAllExpenses);
    this. showNotification();
   } else {
    console.log(this.propertyCharacteristic);
    console.log(this.operatingExpense);    
      this.globalsService.setOperatingExpense(this.operatingExpense);
      this.globalsService.setPropertyCharacteristic(this.propertyCharacteristic);
      this.onSave();
      this.router.navigate(['/home/analysis']); 
   }


   

     
    }

    onSave(){

      this.operatingExpenseDataService.creatOperatingExpense(this.operatingExpense)
      .subscribe(
        (res) => console.log("ajout operatingExpense effectuer dans la base de donné") ,
        (error) => console.log ("bug operatingExpense dans l ajout de la base de donné")
      )
      ;
      this.propertyCharacteristicDataService.creatPropertyCharacteristic(this.propertyCharacteristic).subscribe(

        (res) => console.log("ajout propertyCharacteristic effectuer dans la base de donné") ,
        (error) => console.log ("bug propertyCharacteristic dans l ajout de la base de donné")
      )
    }

    openDialog(): void {

      if (this.total != this.operatingExpense.totalAllExpenses) {
        console.log(this.total);
        console.log(this.operatingExpense.totalAllExpenses);
      this. showNotification();
     } else {
      console.log(this.propertyCharacteristic);
      console.log(this.operatingExpense);    
        this.globalsService.setOperatingExpense(this.operatingExpense);
        this.globalsService.setPropertyCharacteristic(this.propertyCharacteristic);
        this.onSave();
     

     this.operatingExpense = new OperatingExpense () ;
      let dialogRef = this.dialog.open(DataYearComponent, {
        width: '700px',
        data: {  }
      });
      dialogRef.afterClosed().subscribe(res =>{
        this.operatingExpense.yearExpenseData= res ;
      })
  
    }
  }

    showNotification(){
      const type = ['','info','success','warning','danger'];
  
      $.notify({
          icon: "notifications",
          message: "the sum of your Operating Expenses is not equal to your totalAllExpenses"
  
      },{
          type: type[4],
          timer: 4000,
          placement: {
              from: "bottom",
              align: "right"
          }
      });
  }
    
}
