import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupPropertieComponent } from './popup-propertie.component';

describe('PopupPropertieComponent', () => {
  let component: PopupPropertieComponent;
  let fixture: ComponentFixture<PopupPropertieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupPropertieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupPropertieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
