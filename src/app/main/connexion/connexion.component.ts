import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavbarService } from 'app/services/auth/navbar.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {

 
  constructor(public nav: NavbarService) { }

  ngOnInit() {
    this.nav.hide();
  }

}
