import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { AfService } from '../../../services/auth/af.service';
import { User } from '../../../model/User';
import { UserDataService } from '../../../services/data-services/user-data.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  // email:string;
   password:string;
  // firstName:string;
  // lastName:string;
  // job: string;
  user :User = new User();
  constructor(private authService: AfService, private userDataService : UserDataService, private router:Router) { }

  ngOnInit() {
  }

  onSinup(){

    this.authService.register(this.user.email, this.password);

    this.userDataService.creatUser(this.user).subscribe(res => res );
    
    this.router.navigate(['/']);
  }


  hide = true;
 
  jobs = [
    {viewValue: 'Appraiser', value: 'appraiser'},
    {viewValue: 'Property Manager', value: 'propertyManager'},
    {viewValue: 'Building Owner', value: 'buildingOwner'},
    {viewValue: 'Tenant', value: 'tenant'},
    {viewValue: 'Tax Assessor', value: 'taxAssessor'},
    {viewValue: 'Mortgage Broker', value: 'mortgageBroker'},
    {viewValue: 'Real Estate Broker', value: 'realEstateBroker'},
    {viewValue: 'Real Estate Consultant', value: 'realEstateConsultant'},
    {viewValue: 'Developer', value: 'developer'},
  ];
  
}
