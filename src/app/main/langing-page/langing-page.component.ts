import { Component, OnInit, ViewChild } from '@angular/core';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DataYearComponent } from '../popup/data-year/data-year.component';
import { NewPropertyComponent } from 'app/main/new-property/new-property.component';

import { PropertyCharacteristic } from 'app/model/propertyCharacteristic';
import { OperatingExpense } from 'app/model/OperatingExpense';
import { NewAnalysisComponent } from 'app/main/new-analysis/new-analysis.component';
import { NewDataEntryComponent } from 'app/main/new-data-entry/new-data-entry.component';

import { MatStepper } from '@angular/material';
import { OperatingExpenseDataService } from '../../services/data-services/operatingExpense-data.service';
import { PropertyCharacteristicDataService } from '../../services/data-services/propertyCharacteristic-data.service';
import { AnalysisComponent } from 'app/main/analysis/analysis.component';

@Component({
  selector: 'app-langing-page',
  templateUrl: './langing-page.component.html',
  styleUrls: ['./langing-page.component.scss']
})
export class LangingPageComponent implements OnInit {
  @ViewChild("newPropertyComponent") newPropertyComponent: NewPropertyComponent;
  @ViewChild("newAnalysisComponent") newAnalysisComponent: NewAnalysisComponent;
  @ViewChild("newDataEntryComponent") newDataEntryComponent: NewDataEntryComponent;
  @ViewChild("analysisComponent") analysisComponent: AnalysisComponent;

  private operatingExpense  = new OperatingExpense ;
  private propertyCharacteristic   = new PropertyCharacteristic ;

  constructor(public dialog: MatDialog, private operatingExpenseDataService : OperatingExpenseDataService, private propertyCharacteristicDataService : PropertyCharacteristicDataService ) { }

  ngOnInit() {

   
  }

 
    onPropertyCharacteristics(stepper: MatStepper) {
    this.propertyCharacteristic.propertySubType = this.newPropertyComponent.propertyCharacteristic.propertySubType;
    this.propertyCharacteristic.address = this.newPropertyComponent.propertyCharacteristic.address;
    this.propertyCharacteristic.zipCode = this.newPropertyComponent.propertyCharacteristic.zipCode;
    this.propertyCharacteristic.city = this.newPropertyComponent.propertyCharacteristic.city;
    this.propertyCharacteristic.msa = this.newPropertyComponent.propertyCharacteristic.msa;
    this.propertyCharacteristic.gba = this.newPropertyComponent.propertyCharacteristic.gba;
    this.propertyCharacteristic.nra = this.newPropertyComponent.propertyCharacteristic.nra;
    this.propertyCharacteristic.numberUnit = this.newPropertyComponent.propertyCharacteristic.numberUnit;
    this.propertyCharacteristic.averageUnitSize = this.newPropertyComponent.propertyCharacteristic.averageUnitSize;
    this.propertyCharacteristic.rentableArea = this.newPropertyComponent.propertyCharacteristic.rentableArea;
    this.propertyCharacteristic.numberTenants = this.newPropertyComponent.propertyCharacteristic.numberTenants;
    this.propertyCharacteristic.yoc = this.newPropertyComponent.propertyCharacteristic.yoc;
    this.propertyCharacteristic.investmentClass = this.newPropertyComponent.propertyCharacteristic.investmentClass;
    this.propertyCharacteristic.yearBuilt = this.newPropertyComponent.propertyCharacteristic.yearBuilt;
    this.propertyCharacteristic.yearMostRenovation = this.newPropertyComponent.propertyCharacteristic.yearMostRenovation;
    this.propertyCharacteristic.surface = (this.propertyCharacteristic.numberUnit *  this.propertyCharacteristic.averageUnitSize);
    console.log(this.propertyCharacteristic);
    console.log(this.operatingExpense);
    stepper.next();
  
  }
  onOperatingExpensesDataEntry(stepper: MatStepper) {
  this.operatingExpense.effectiveGrossIncome = this.newDataEntryComponent.operatingExpense.effectiveGrossIncome;
  this.operatingExpense.administration = this.newDataEntryComponent.operatingExpense.administration;
  this.operatingExpense.utilities = this.newDataEntryComponent.operatingExpense.utilities;
  this.operatingExpense.payroll = this.newDataEntryComponent.operatingExpense.payroll;
  this.operatingExpense.repairsMaintenance = this.newDataEntryComponent.operatingExpense.repairsMaintenance;
  this.operatingExpense.cleaningJanitorial = this.newDataEntryComponent.operatingExpense.cleaningJanitorial;
  this.operatingExpense.cam = this.newDataEntryComponent.operatingExpense.cam;
  this.operatingExpense.realEstateTaxes = this.newDataEntryComponent.operatingExpense.realEstateTaxes;
  this.operatingExpense.insurance = this.newDataEntryComponent.operatingExpense.insurance;
  this.operatingExpense.replacementReserves = this.newDataEntryComponent.operatingExpense.replacementReserves;  
  this.operatingExpense.totalAllExpenses = this.newDataEntryComponent.operatingExpense.totalAllExpenses;
  // this.operatingExpense.totalAllExpenses = (this.operatingExpense.replacementReserves + this.operatingExpense.insurance + this.operatingExpense.realEstateTaxes + this.operatingExpense.cam +  this.operatingExpense.cleaningJanitorial +  this.operatingExpense.repairsMaintenance +  this.operatingExpense.payroll + this.operatingExpense.utilities + this.operatingExpense.administration + this.operatingExpense.effectiveGrossIncome )
  this.operatingExpense.netOperatingIncome = this.newDataEntryComponent.operatingExpense.netOperatingIncome;
  
  this.analysisComponent.operatingExpense.effectiveGrossIncome = this.newDataEntryComponent.operatingExpense.effectiveGrossIncome;
  this.analysisComponent.operatingExpense.administration = this.newDataEntryComponent.operatingExpense.administration;
  this.analysisComponent.operatingExpense.utilities = this.newDataEntryComponent.operatingExpense.utilities;
  this.analysisComponent.operatingExpense.payroll = this.newDataEntryComponent.operatingExpense.payroll ;
  this.analysisComponent.operatingExpense.repairsMaintenance =   this.newDataEntryComponent.operatingExpense.repairsMaintenance;
  this.analysisComponent.operatingExpense.cleaningJanitorial =   this.newDataEntryComponent.operatingExpense.cleaningJanitorial;
  this.analysisComponent.operatingExpense.cam =  this.newDataEntryComponent.operatingExpense.cam;
  this.analysisComponent.operatingExpense.realEstateTaxes = this.newDataEntryComponent.operatingExpense.realEstateTaxes;
  this.analysisComponent.operatingExpense.insurance = this.newDataEntryComponent.operatingExpense.insurance ;
  this.analysisComponent.operatingExpense.replacementReserves = this.newDataEntryComponent.operatingExpense.replacementReserves ;
 this.analysisComponent.total  =  (this.analysisComponent.operatingExpense.administration + this.analysisComponent.operatingExpense.utilities + 
this.analysisComponent.operatingExpense.payroll +
this.analysisComponent.operatingExpense.repairsMaintenance + this.operatingExpense.cleaningJanitorial +
this.analysisComponent.operatingExpense.cam +
this.analysisComponent.operatingExpense.realEstateTaxes +
this.analysisComponent.operatingExpense.insurance +
this.analysisComponent.operatingExpense.replacementReserves
);  
 this.analysisComponent.operatingExpense.totalAllExpenses =  this.operatingExpense.totalAllExpenses ;
  this.analysisComponent.operatingExpense.netOperatingIncome =   this.newDataEntryComponent.operatingExpense.netOperatingIncome;

this.onSave();
    stepper.next();
    }
    onTest() {
      console.log( this.analysisComponent.total)
  }
    onSave(){

      this.operatingExpenseDataService.creatOperatingExpense(this.operatingExpense)
      .subscribe(
        (res) => console.log("ca marche pour operatingExpenseDataService ") ,
        (error) => console.log ("ca marche pas ")
      )
      ;
      this.propertyCharacteristicDataService.creatPropertyCharacteristic(this.propertyCharacteristic).subscribe(

        (res) => console.log("ca marche pour propertyCharacteristicDataService ") ,
        (error) => console.log ("ca marche pas propertyCharacteristicDataService ")
      )
    }

  openDialog(): void {
    let dialogRef = this.dialog.open(DataYearComponent, {
      width: '700px',
      data: {  }
    });
    dialogRef.afterClosed().subscribe(res =>{
      console.log(res);
      this.operatingExpenseDataService.creatOperatingExpense(this.operatingExpense)
      .subscribe(
        (res) => console.log("ca marche pour operatingExpenseDataService ") ,
        (error) => console.log ("ca marche pas ")
      )
      ;
    })


}

}