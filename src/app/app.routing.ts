import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';

//component 


import { Error404Component } from './error/error404/error404.component';
import { Error500Component } from './Error/error500/error500.component';
import { ConnexionComponent } from './main/connexion/connexion.component';

import { ListPropertieComponent } from './main/properties/list-propertie/list-propertie.component';
import { NewAnalysisComponent } from './main/new-analysis/new-analysis.component';
import { NewPropertyComponent } from './main/new-property/new-property.component';
import { NewDataEntryComponent } from './main/new-data-entry/new-data-entry.component';
import { AnalysisComponent } from './main/analysis/analysis.component';
import { ListUserComponent } from './main/list-user/list-user.component';
import { MyreportsComponent } from './main/myreports/myreports.component';
import { LangingPageComponent } from './main/langing-page/langing-page.component';
import { MydataComponent } from './main/mydata/mydata.component';
import { AuthGuard } from 'app/services/guards/auth.guard';
import { MapComponent } from './main/map/map.component';
import { OpRedataComponent } from './main/op-redata/op-redata.component';
import { SalesComponent } from './main/sales/sales.component';



const routes: Routes =[
  
    {path : '', component:ConnexionComponent},    
    {path : 'home',component:LangingPageComponent, children: [
      {path: '', redirectTo: 'new-analysis', pathMatch: 'full'},
      { path: 'new-analysis', component: NewAnalysisComponent },
      { path: 'new-property', component: NewPropertyComponent },
      { path: 'new-data-entry', component: NewDataEntryComponent },
      { path: 'analysis', component: AnalysisComponent }
    ],canActivate:[AuthGuard]},
    {path : 'opredata', component:OpRedataComponent, canActivate:[AuthGuard]},
    {path : 'sales', component:SalesComponent, canActivate:[AuthGuard]}, 
    {path : 'mydata', component:MydataComponent, canActivate:[AuthGuard]},  
    {path : 'errors/error-404', component:Error404Component, canActivate:[AuthGuard]},
    {path : 'errors/error-500', component:Error500Component, canActivate:[AuthGuard]},
    {path : '**',redirectTo: 'errors/error-404' }
    
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{useHash:true})
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
