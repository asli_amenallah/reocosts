import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { OperatingExpense } from '../../model/OperatingExpense';
import 'rxjs/Rx';
@Injectable()
export class OperatingExpenseDataService {

  constructor(private http: Http) { }

creatOperatingExpense(operatingExpense: OperatingExpense){
    return this.http.post('https://webapplication-201615.firebaseio.com/operatingExpense.json', operatingExpense);
  }

  getOperatingExpense(){
    return this.http.get("https://webapplication-201615.firebaseio.com/operatingExpense.json")
    .map(response => response.json())
}

getOneOperatingExpense(id){
    return this.http.get("https://webapplication-201615.firebaseio.com/operatingExpense/"+id+".json")
    .map(response => response.json())
}

}
