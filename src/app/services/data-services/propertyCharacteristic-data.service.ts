import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { PropertyCharacteristic } from '../../model/propertyCharacteristic';
import 'rxjs/Rx';
@Injectable()
export class PropertyCharacteristicDataService {

  constructor(private http: Http) { }

creatPropertyCharacteristic(propertyCharacteristic: PropertyCharacteristic){
    return this.http.post('https://webapplication-201615.firebaseio.com/propertyCharacteristic.json', propertyCharacteristic);
  }

  getPropertyCharacteristic(){
    return this.http.get("https://webapplication-201615.firebaseio.com/propertyCharacteristic.json")
    .map(response => response.json())
}

getOnePropertyCharacteristic(id){
    return this.http.get("https://webapplication-201615.firebaseio.com/propertyCharacteristic/"+id+".json")
    .map(response => response.json())
}

}
