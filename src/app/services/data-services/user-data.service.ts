import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
@Injectable()
export class UserDataService {

  constructor(private http: Http) { }

creatUser(user){
    return this.http.post('https://webapplication-201615.firebaseio.com/user.json', user);
  }

  getUser(){
    return this.http.get("https://webapplication-201615.firebaseio.com/user.json")
    .map(response => response.json())
}


}
