import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';

import * as firebase from 'firebase/app'
import { Observable } from 'rxjs/Observable';
import { User } from '../../model/User';

import { switchMap } from 'rxjs/operators';
import { merge } from 'rxjs-compat/operator/merge';

@Injectable()
export class AfService {
    user$: Observable<User>;
    constructor (public afAuth: AngularFireAuth, public afS: AngularFirestore){

        this.user$ = afAuth.authState.switchMap ( user => {

            if (user) {
                return this.afS.doc<User>('users/$(user.uid)').valueChanges()
            } else {
                return Observable.of(null);
            }
        })
    }
    
    login(email:string, password:string){
        return new Promise((resolve, reject) => {
          this.afAuth.auth.signInWithEmailAndPassword(email, password)
            .then(userData => resolve(userData),
            err => reject(err));
        });
      }
    
    loginWithGoogle(){
        const provider = new firebase.auth.GoogleAuthProvider();
        this.afAuth.auth.signInWithPopup(provider).then((credential=> {
            this.updateUser(credential.user)
        }));}


    updateUser(user){
        const userRef : AngularFirestoreDocument<any> = this.afS.doc('users/$(user.uid)');
        const data : User = { 
            uid: user.uid,
            firstName: user.firstName,
            lastName: user.lastName,
            type: {
                dev: true,
                admin: false},
            city: user.city,
            email: user.email,
        }
        return userRef.set(data, {merge : true});
    }


    getAuth(){
        return this.afAuth.authState.map(auth => auth);
      }

  register(email:string, password:string){
    return new Promise((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(email, password)
        .then(userData => resolve(userData),
          err => reject(err));
    });
  }

    logout(){
        this.afAuth.auth.signOut();
    }
}