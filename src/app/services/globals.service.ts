import { Injectable } from '@angular/core';
import { PropertyCharacteristic } from 'app/model/propertyCharacteristic';
import { OperatingExpense } from 'app/model/OperatingExpense';

@Injectable()
export class GlobalsService {

  constructor() { }
  
   oeratingExpenseVariable = new OperatingExpense();
  private propertyCharacteristicVariable = new  PropertyCharacteristic();

  getOperatingExpense(){
    return this.oeratingExpenseVariable;
  }
  getPropertyCharacteristic(){
    return this.propertyCharacteristicVariable ;
  }
  setOperatingExpense(oeratingExpenseVariable : OperatingExpense){
    return this.oeratingExpenseVariable = oeratingExpenseVariable;
  }
  setPropertyCharacteristic(propertyCharacteristicVariable: PropertyCharacteristic){
    return this.propertyCharacteristicVariable = propertyCharacteristicVariable;
  }

}
