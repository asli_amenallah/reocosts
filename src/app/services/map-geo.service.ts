import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

import * as GeoFire from "geofire";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class MapGeoService {
dbRef : any;
geoFire : any;

hits = new BehaviorSubject([])

  constructor(private db : AngularFireDatabase) { 

    this.dbRef = this.db.list('/webapplication-201615');
    this.geoFire = new GeoFire(this.dbRef.$ref);
  }



setLocation(key : string, coords: Array<number>){

this.geoFire.set(key, coords)
.then(_ => console.log('location updated'))
.catch(err => console.log(err))

}



}
