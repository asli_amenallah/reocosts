import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { AppComponent } from './app.component';
// firebase npm install angularfire2 firebase --save
import { environment } from './../environments/environment'
import { AngularFireModule } from './../../node_modules/angularfire2';
import { AngularFireDatabaseModule } from './../../node_modules/angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';


//component
import { DashboardComponent } from './dashboard/dashboard.component';
import { Error404Component } from './error/error404/error404.component';
import { Error500Component } from './Error/error500/error500.component';
import { ConnexionComponent } from './main/connexion/connexion.component';
import { LoginComponent } from './main/connexion/login/login.component';
import { SignupComponent } from './main/connexion/signup/signup.component';

//Material design
import { AppMaterial } from 'app/app-material';
//editor de text npm install angular-froala-wysiwyg --save
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
//   npm install ng2-currency-mask --save
import { CurrencyMaskModule } from "ng2-currency-mask";
//services

import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
//pipe
import { KeysPipe } from 'app/services/pipes/keys.pipe';
// npm install @agm/core --save
import { AgmCoreModule } from '@agm/core';


import { NavbarService } from 'app/services/auth/navbar.service';
import { PropertiesComponent } from './main/properties/properties.component';
import { ListPropertieComponent } from './main/properties/list-propertie/list-propertie.component';
import { PopupPropertieComponent } from './main/properties/popup-propertie/popup-propertie.component';
import { MydataComponent } from './main/mydata/mydata.component';
import { ListMydataComponent } from './main/mydata/list-mydata/list-mydata.component';
import { PopupMydataComponent } from './main/mydata/popup-mydata/popup-mydata.component';
import { AddMydataComponent } from './main/mydata/add-mydata/add-mydata.component';
import { MyreportsComponent } from './main/myreports/myreports.component';
import { ListReportsComponent } from './main/myreports/list-reports/list-reports.component';
import { NewAnalysisComponent } from './main/new-analysis/new-analysis.component';
import { NewPropertyComponent } from './main/new-property/new-property.component';
import { NewDataEntryComponent } from './main/new-data-entry/new-data-entry.component';
import { AnalysisComponent } from './main/analysis/analysis.component';
import { ListUserComponent } from './main/list-user/list-user.component';
import { LangingPageComponent } from './main/langing-page/langing-page.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { DataYearComponent } from './main/popup/data-year/data-year.component';
import { PropertyCharacteristicDataService } from 'app/services/data-services/propertyCharacteristic-data.service';
import { OperatingExpenseDataService } from 'app/services/data-services/operatingExpense-data.service';
import { GlobalsService } from 'app/services/globals.service';
import { AfService } from 'app/services/auth/af.service';
import { AuthGuard } from 'app/services/guards/auth.guard';
import { UserDataService } from 'app/services/data-services/user-data.service';
import { MapComponent } from 'app/main/map/map.component';
import { OpRedataComponent } from './main/op-redata/op-redata.component';
import { SalesComponent } from './main/sales/sales.component';
import { ProjectComponent } from './main/project/project.component';

@NgModule({
  declarations: [
    AppComponent,
    KeysPipe,
    DashboardComponent,
    Error404Component,
    Error500Component,
    ConnexionComponent,
    LoginComponent,
    SignupComponent,
    PropertiesComponent,
    ListPropertieComponent,
    PopupPropertieComponent,
    MydataComponent,
    ListMydataComponent,
    PopupMydataComponent,
    AddMydataComponent,
    MyreportsComponent,
    ListReportsComponent,
    NewAnalysisComponent,
    NewPropertyComponent,
    NewDataEntryComponent,
    AnalysisComponent,
    ListUserComponent,
    LangingPageComponent,
    DataYearComponent,
    MapComponent,
    OpRedataComponent,
    SalesComponent,
    ProjectComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppMaterial,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAOAB3zEWzZok5s45SvIxBf9u1SIY1rf8A'
    }),
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  providers: [AuthGuard, AfService, PropertyCharacteristicDataService, OperatingExpenseDataService ,UserDataService, GlobalsService, NavbarService ,{provide: LocationStrategy, useClass: HashLocationStrategy}

  ],
  entryComponents: [
  
    DataYearComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
