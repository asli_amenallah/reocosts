// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase :{
    apiKey: 'AIzaSyAOAB3zEWzZok5s45SvIxBf9u1SIY1rf8A',
    authDomain: 'webapplication-201615.firebaseapp.com',
    databaseURL: 'https://webapplication-201615.firebaseio.com',
    projectId: 'webapplication-201615',
    storageBucket: 'webapplication-201615.appspot.com',
    messagingSenderId: '887614638047'
  },
  googleMapsKey:'AIzaSyAOAB3zEWzZok5s45SvIxBf9u1SIY1rf8A'
};
